# Dominion-mtg

A game inspired by deck-building (Dominion...) and the mana system from MtG.

Tech: boardgame.io and React

## Launch

```bash
# Once
yarn

# Start
yarn start
```
