{
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:@typescript-eslint/recommended",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended"
  ],
  "plugins": [
    "@typescript-eslint",
    "prettier"
  ],
  "ignorePatterns": "dist/**/*",
  "env": {
    "browser": true,
    "jasmine": true,
    "jest": true,
    "es6": true,
    "node": true
  },
  "rules": {
    "prettier/prettier": [
      "error",
      {
        "singleQuote": true,
        "printWidth": 120,
        "endOfLine": "auto"
      }
    ],
    "sort-imports": [
      "warn",
      {
        "ignoreDeclarationSort": true,
        "memberSyntaxSortOrder": [
          "none",
          "all",
          "single",
          "multiple"
        ]
      }
    ],
    "@typescript-eslint/explicit-function-return-type": [
      "warn",
      {
        "allowExpressions": true,
        "allowTypedFunctionExpressions": true
      }
    ],
    "@typescript-eslint/no-extra-non-null-assertion": [
      "warn"
    ],
    "@typescript-eslint/no-extraneous-class": [
      "warn",
      {
        "allowStaticOnly": true,
        "allowWithDecorator": true
      }
    ],
    "@typescript-eslint/no-non-null-assertion": [
      "off"
    ],
    "@typescript-eslint/no-non-null-asserted-optional-chain": [
      "warn"
    ],
    "@typescript-eslint/no-require-imports": [
      "warn"
    ],
    "@typescript-eslint/no-unnecessary-boolean-literal-compare": [
      "warn"
    ],
    "@typescript-eslint/no-unnecessary-qualifier": [
      "warn"
    ],
    "@typescript-eslint/no-throw-literal": [
      "warn"
    ],
    "@typescript-eslint/no-implied-eval": [
      "warn"
    ],
    "@typescript-eslint/no-floating-promises": [
      "warn"
    ],
    "@typescript-eslint/member-ordering": [
      "warn"
    ],
    "@typescript-eslint/prefer-for-of": [
      "warn"
    ],
    "@typescript-eslint/prefer-function-type": [
      "warn"
    ],
    "@typescript-eslint/prefer-nullish-coalescing": [
      "warn"
    ],
    "@typescript-eslint/prefer-optional-chain": [
      "warn"
    ],
    "@typescript-eslint/prefer-readonly": [
      "warn"
    ],
    "@typescript-eslint/require-array-sort-compare": [
      "warn",
      {
        "ignoreStringArrays": true
      }
    ],
    "@typescript-eslint/restrict-plus-operands": [
      "warn"
    ],
    "@typescript-eslint/restrict-template-expressions": [
      "warn",
      {
        "allowNumber": true,
        "allowNullable": true,
        "allowBoolean": true
      }
    ],
    "@typescript-eslint/switch-exhaustiveness-check": [
      "warn"
    ],
    "@typescript-eslint/unified-signatures": [
      "warn"
    ],
    "react/prop-types": [
      "off"
    ],
    "react/no-unescaped-entities": [
      "error",
      {
        "forbid": [
          ">",
          "\"",
          "}"
        ]
      }
    ]
  },
  "settings": {
    "react": {
      "pragma": "React",
      "version": "detect"
    }
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "project": [
      "./tsconfig.json"
    ],
    "tsconfigRootDir": "./"
  }
}
