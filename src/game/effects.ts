/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { useEffectListener } from 'bgio-effects/dist/react';
import { EffectsPluginConfig } from 'bgio-effects/dist/types';
import { PlayerID } from 'boardgame.io';
import { DependencyList } from 'react';
import { Rune } from './Rune';
import { Log } from './state/Log';

interface EffectConfig<P> {
  create: (arg: P) => P;
  duration?: number;
}

function simpleEffect<P = undefined>(durationMs?: number): EffectConfig<P> {
  return {
    create: (payload: P) => payload,
    duration: durationMs === undefined ? undefined : durationMs / 1000 + 0.2,
  };
}

export const DISCARD_EFFECT_DURATION = 500;
export const DRAW_EFFECT_DURATION = 500;
export const GAIN_RUNE_EFFECT_DURATION = 100;
export const DESTROYED_RUNE_EFFECT_DURATION = 500;

const _effectsPluginConfig = {
  effects: {
    lostHealth: simpleEffect<{ playerID: PlayerID; amount: number }>(),
    gainedHealth: simpleEffect<{ playerID: PlayerID; amount: number }>(),
    // no duration because we need the state to already be updated for the animation to work
    drewRune: simpleEffect<{ playerID: PlayerID; rune: Rune; all: boolean }>(),
    discardedRune: simpleEffect<{ playerID: PlayerID; rune: Rune }>(DISCARD_EFFECT_DURATION),
    gainedRune: simpleEffect<{ playerID: PlayerID; rune: Rune }>(GAIN_RUNE_EFFECT_DURATION),
    destroyedRuneFromReserve: simpleEffect<{ playerID: PlayerID; rune: Rune }>(DESTROYED_RUNE_EFFECT_DURATION),
    destroyedRuneFromHand: simpleEffect<{ playerID: PlayerID; rune: Rune }>(DESTROYED_RUNE_EFFECT_DURATION),
    triggeredEnchantment: simpleEffect<{ playerID: PlayerID }>(),

    newLog: simpleEffect<{ log: Log }>(),

    beginTurn: simpleEffect<{ playerID: PlayerID }>(),
  },
};

type Effects = typeof _effectsPluginConfig['effects'];
type Payload<K extends keyof Effects> = Parameters<Effects[K]['create']>[0];
export type CtxEffectsType = {
  readonly [K in keyof Effects]: (payload: Payload<K>, position?: string, duration?: number) => void;
};

export const effectsPluginConfig: EffectsPluginConfig = _effectsPluginConfig;

type CbReturn = void | (() => void);
export type MyUseEffectsListener = <E extends keyof Effects>(
  event: E,
  cb: (payload: Payload<E>) => CbReturn,
  deps: DependencyList
) => void;

export const useMyEffectListener = useEffectListener as MyUseEffectsListener;

type MyUseBuiltinEffectsListener = (
  event: 'effects:start' | 'effects:end',
  cb: () => CbReturn,
  deps: DependencyList
) => void;
export const useMyBuiltinEffectListener = useEffectListener as MyUseBuiltinEffectsListener;
