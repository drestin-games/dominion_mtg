import { EffectsPlugin } from 'bgio-effects/plugin';
import { Ctx, Game } from 'boardgame.io';
import { O } from 'ts-toolbelt';
import { Immutable, Mutability } from './utils';
import { castSpell, discardMove, passTurn, shuffleHand, submitGiftsChoice } from './action/moves';
import { CtxEffectsType, effectsPluginConfig } from './effects';
import { EndState } from './EndState';
import { initState } from './state/init';
import { MyGameState } from './state/State';
import { ActivePlayers } from 'boardgame.io/core';

// Removed type to be able to infer type of moves.
const _MyGame = {
  // const _MyGame: Game<MyGameState<'mut'>, ModifiedCtx> = {
  name: 'my-game',
  setup: initState,
  moves: {
    passTurn,
    castSpell,
    shuffleHand,
    discardMove,
    submitGiftsChoice,
  },
  turn: {
    activePlayers: ActivePlayers.ALL,
  },

  minPlayers: 2,

  endIf: (G: MyGameState): EndState | undefined => G.gameover,

  plugins: [EffectsPlugin(effectsPluginConfig)],
};
export const MyGame: Game<MyGameState<'mut'>, ModifiedCtx> = _MyGame;

type ModifiedCtx = O.Merge<Ctx, { readonly effects?: CtxEffectsType }>;

type ReadonlyCtx = Immutable<
  O.Merge<
    Pick<
      ModifiedCtx,
      | 'activePlayers'
      | 'currentPlayer'
      | 'phase'
      | 'playOrder'
      | 'numMoves'
      | 'numPlayers'
      | 'playOrderPos'
      | 'playerID'
      | 'turn'
    >,
    { gameover?: EndState }
  >
>;
type MutableCtx = O.Merge<ReadonlyCtx, Pick<ModifiedCtx, 'events' | 'random' | 'effects'>>;
export type MyCtx<M extends Mutability = 'const'> = M extends 'const' ? ReadonlyCtx : MutableCtx;

type Moves = typeof _MyGame['moves'];
type MoveFunction<M> = M extends { move: (G: MyGameState<'mut'>, ctx: MyCtx<'mut'>, ...args: infer Args) => void }
  ? (...args: Args) => void
  : M extends (G: MyGameState<'mut'>, ctx: MyCtx<'mut'>, ...args: infer Args) => void
  ? (...args: Args) => void
  : never;
export type CtxMovesType = { [K in keyof Moves]: MoveFunction<Moves[K]> };
