import { useEffect, useState } from 'react';
import { A, F, L, M, O } from 'ts-toolbelt';

/** Deep Readonly on List and Object */
export type Immutable<T> = T extends L.List
  ? L.Readonly<T, 'deep'>
  : T extends O.Object
  ? O.Readonly<T, A.Key, 'deep'>
  : T;

export type Mutable<T> = T extends L.List
  ? L.Writable<T, 'deep'>
  : T extends O.Object
  ? O.Writable<T, A.Key, 'deep'>
  : T;

type Jsonify<T> = T extends F.Function
  ? never
  : T extends M.JSON.Primitive
  ? T
  : T extends Array<infer K>
  ? Jsonify<K> extends never
    ? never
    : T
  : T extends { [K in keyof T]: Jsonify<T[K]> }
  ? T
  : never;

export function deepCopy<T>(value: T): Jsonify<T> {
  return JSON.parse(JSON.stringify(value)) as Jsonify<T>;
}

type Jsonable = M.JSON.Primitive | ReadonlyArray<Jsonable> | { [k: string]: Jsonable | undefined };

export function deepEqual<T extends unknown>(a: T, b: T): boolean {
  if (a === b) {
    return true;
  }

  if (b instanceof Array) {
    return a instanceof Array && b instanceof Array && a.length === b.length && a.every((v, i) => deepEqual(b[i], v));
  }

  return (
    a instanceof Object &&
    b instanceof Object &&
    Object.keys(a).length === Object.keys(b).length &&
    Object.entries(a).every(([k, v]) => deepEqual(v, (b as Record<string, unknown>)[k]))
  );
}

export type Mutability = 'const' | 'mut';
export type Access<TMut, M extends Mutability> = M extends 'const' ? Immutable<TMut> : TMut;

export interface Position {
  readonly x: number;
  readonly y: number;
}

export function useDisplayedState<T extends Jsonable>(trueState: T): [T, React.Dispatch<React.SetStateAction<T>>] {
  const [displayedState, setDisplayedState] = useState<T>(trueState);

  useEffect(() => {
    if (!deepEqual(displayedState, trueState)) {
      console.warn(
        `Displayed state was different from true state: ${JSON.stringify(displayedState)} ${JSON.stringify(trueState)}`
      );
      setDisplayedState(trueState);
    }
  }, [trueState]);

  return [displayedState, setDisplayedState];
}

export function arrayUnique<T>(array: ReadonlyArray<T>, eq: (a: T, b: T) => boolean = deepEqual): boolean {
  if (array.length <= 1) {
    return true;
  }
  return array.every((value, i) => {
    return array.slice(i + 1).every((v) => !eq(value, v));
  });
}

export function arrayCount<T>(array: ReadonlyArray<T>, predicate: (elem: T) => boolean): number {
  let count = 0;
  for (const arrayElem of array) {
    if (predicate(arrayElem)) {
      count += 1;
    }
  }
  return count;
}

export function pseudoGuid(): string {
  const S4 = (): string => {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  };
  return S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + S4() + S4();
}

export function appendFunctions<F extends (...args: ReadonlyArray<unknown>) => void>(f: F, g: F): F {
  return ((...args) => {
    f(args);
    g(args);
  }) as F;
}
