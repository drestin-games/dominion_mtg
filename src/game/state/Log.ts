/* eslint-disable @typescript-eslint/ban-types */
import { PlayerID } from 'boardgame.io';
import { A } from 'ts-toolbelt';
import { Rune } from '../Rune';
import { Enchantment } from '../spell/Enchantment';
import { SpellID } from '../spell/Spell';

type _Log<Type extends string, Payload extends Record<string, unknown> = {}> = A.Compute<
  {
    type: Type;
  } & Payload
>;

type PlayerLog<Type extends string, Others extends Record<string, unknown> = {}> = _Log<
  Type,
  {
    playerID: PlayerID;
  } & Others
>;

type Shuffle = PlayerLog<'shuffle'>;
type UseShuffle = PlayerLog<'use-shuffle'>;
type GainShuffleUses = PlayerLog<'gain-shuffle-uses', { amount: number }>;
type GainCasts = PlayerLog<'gain-casts', { amount: number }>;
type Cast = PlayerLog<'cast', { spellID: SpellID }>;
type Discard = PlayerLog<'discard', { runes: readonly Rune[] }>;
type Draw = PlayerLog<'draw', { runes: readonly Rune[] }>;
type GainRunes = PlayerLog<'gain-runes', { runes: readonly Rune[] }>;
type DestroyRunesFromHand = PlayerLog<'destroy-runes-from-hand', { runes: readonly Rune[] }>;
type DestroyRunesFromReserve = PlayerLog<'destroy-runes-from-reserve', { runes: readonly Rune[] }>;
type GainHealth = PlayerLog<'gain-health', { amount: number }>;
type LoseHealth = PlayerLog<'lose-health', { amount: number }>;
type SufferDamage = PlayerLog<'suffer-damage', { amount: number }>;
type PassTurn = PlayerLog<'pass-turn', { noMoreCasts: boolean }>;
type NewRound = _Log<'new-round'>;
type AddEnchantment = PlayerLog<'add-enchantment', { enchantment: Enchantment }>;
type EnchantmentExpire = PlayerLog<'enchantment-expire', { enchantment: Enchantment }>;
type TriggerEnchantment = PlayerLog<'trigger-enchantment', { enchantment: Enchantment }>;
type BeginTurn = PlayerLog<'begin-turn'>;

export type Log =
  | Shuffle
  | UseShuffle
  | GainShuffleUses
  | GainCasts
  | Cast
  | Discard
  | Draw
  | GainRunes
  | DestroyRunesFromHand
  | DestroyRunesFromReserve
  | GainHealth
  | LoseHealth
  | SufferDamage
  | PassTurn
  | NewRound
  | AddEnchantment
  | EnchantmentExpire
  | TriggerEnchantment
  | BeginTurn;
