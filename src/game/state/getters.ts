import { PlayerID } from 'boardgame.io';
import { Mutability } from '../utils';
import { DISCARD_MOVE_HP_COST } from '../action/moves';
import { MyCtx } from '../Game';
import { Rune, RuneID, RuneType } from '../Rune';
import { areCastConditionsSastified } from '../spell/CastConditions';
import { runesCanPayCost } from '../spell/Cost';
import { RuneParam, Who } from '../spell/ParamDef';
import { Spell, SpellID, getSpell } from '../spell/Spell';
import { Hand, MyGameState, PlayerState, Reserve } from './State';

export interface HandRange {
  from: RuneID;
  to: RuneID;
}

export function rangeToString({ from, to }: HandRange): string {
  return `[${from}, ${to}]`;
}

export function isInRange(playerID: PlayerID, range: HandRange, runeID: RuneID, G: MyGameState): boolean {
  return rangeToRuneIDs(playerID, range, G).includes(runeID);
}

export function rangeFromOne(runeID: RuneID): HandRange {
  return {
    from: runeID,
    to: runeID,
  };
}

export function findRuneIndex(list: Hand | Reserve, runeID: RuneID): number | null {
  const index = list.findIndex((r) => r.id === runeID);
  return index >= 0 ? index : null;
}

/**
 * Return the runes composing the hand range, is the right order.
 */
export function rangeToRuneIDs(playerID: PlayerID, range: HandRange, G: MyGameState): RuneID[] {
  const hand = getPlayerState(playerID, G).hand;
  const indexFrom = findRuneIndex(hand, range.from);
  const indexTo = findRuneIndex(hand, range.to);
  if (indexFrom === null || indexTo === null) {
    throw new Error(`Invalid range ${rangeToString(range)} for player ${playerID}`);
  }
  const minIndex = Math.min(indexFrom, indexTo);
  const maxIndex = Math.max(indexFrom, indexTo);

  const runeIDs: RuneID[] = [];
  for (let i = minIndex; i <= maxIndex; i++) {
    runeIDs.push(hand[i].id);
  }
  return runeIDs;
}

/**
 * Find living oponents of the given player
 */
export function getOponents(playerID: PlayerID, G: MyGameState): PlayerID[] {
  return getLivingPlayerIDs(G).filter((id) => id != playerID);
}

/**
 * Return a reference to the state of the given player.
 */
export function getPlayerState<M extends Mutability = 'const'>(playerID: PlayerID, G: MyGameState<M>): PlayerState<M> {
  return G.players[playerID] as PlayerState<M>; // FIXME
}

/**
 * Return a reference to the state of the current player.
 */
export function getCurrentPlayerState<M extends Mutability = 'const'>(G: MyGameState<M>): PlayerState<M> {
  return getPlayerState<M>(G.turn.currentPlayerID, G);
}

/**
 * If the given rune is in the given player hand.
 */
export function isRuneInHand(playerID: PlayerID, runeID: RuneID, G: MyGameState): boolean {
  return findRuneIndex(getPlayerState(playerID, G).hand, runeID) !== null;
}

/**
 * If the given rune is in the given player reserve.
 */
export function isRuneInReserve(playerID: PlayerID, runeID: RuneID, G: MyGameState): boolean {
  return findRuneIndex(getPlayerState(playerID, G).reserve, runeID) !== null;
}

/**
 * Return if the given range is valid for the given player.
 */
export function isValidRange(playerID: PlayerID, range: HandRange, G: MyGameState): boolean {
  return isRuneInHand(playerID, range.from, G) && isRuneInHand(playerID, range.to, G);
}

/**
 * Return if the given string is a valid player ID (not dead).
 */
export function playerExists(playerID: string, G: MyGameState): playerID is PlayerID {
  return getLivingPlayerIDs(G).includes(playerID);
}

/**
 * Return an array of runes corresponding to the slice [from, to] of the given player's hand.
 */
export function getHandRunes(playerID: PlayerID, range: HandRange, G: MyGameState): Rune[] {
  const hand = getPlayerState(playerID, G).hand;
  if (!isValidRange(playerID, range, G)) {
    throw new Error(`Invalid hand range: ${rangeToString(range)} for hand ${JSON.stringify(hand)}`);
  }
  return rangeToRuneIDs(playerID, range, G).map((runeID) => hand.find((b) => runeID === b.id)!);
}

/**
 * Return the IDs of every player (even dead).
 */
export function getAllPlayerIDs(G: MyGameState): PlayerID[] {
  return Object.keys(G.players);
}

/**
 * Return the IDs of every living player.
 */
export function getLivingPlayerIDs(G: MyGameState): PlayerID[] {
  return getAllPlayerIDs(G).filter((id) => !isDead(id, G));
}

/**
 * Return an array of the ids and states of all players.
 */
export function getAllPlayers<M extends Mutability = 'const'>(
  G: MyGameState<M>
): { playerID: PlayerID; playerState: PlayerState<M> }[] {
  return Object.entries(G.players).map(([playerID, playerState]) => ({
    playerID,
    playerState: playerState as PlayerState<M>, // FIXME type workaround
  }));
}

/**
 * Can the given player pay the given spell with the runes of the given range.
 */
export function canPaySpell(playerID: PlayerID, spellID: SpellID, range: HandRange, G: MyGameState): boolean {
  return runesCanPayCost(getSpell(spellID).cost, getHandRunes(playerID, range, G));
}

/**
 * Return the number of runes in the given reserve.
 */
export function runeCount(reserve: Reserve): number {
  return reserve.length;
}

/**
 * Return the number of runes of the given type in the reserve.
 */
export function countRunesOfType(type: RuneType, reserve: Reserve): number {
  return reserve.filter((r) => r.type === type).length;
}

/**
 * Return if the given player sastify the who requirement.
 */
export function isValidPlayer(playerID: PlayerID, who: Who, G: MyGameState): boolean {
  return (
    !isDead(playerID, G) &&
    (who === 'anyone' ||
      (who === 'self' && playerID === G.turn.currentPlayerID) ||
      (who === 'oponent' && playerID !== G.turn.currentPlayerID))
  );
}

/**
 * If the cast conditions of the given spell are satisfied by the given player.
 */
export function canCastSpell(playerID: PlayerID, spell: Spell, G: MyGameState): boolean {
  const player = getPlayerState(playerID, G);
  return areCastConditionsSastified(spell.castConditions, spell.id, player.castedSpells);
}

/**
 * If the given player is dead.
 */
export function isDead(playerID: PlayerID, G: MyGameState): boolean {
  return getPlayerState(playerID, G).health <= 0;
}

/**
 * If the player can discard.
 */
export function canUseDiscardMove(playerID: PlayerID, G: MyGameState): boolean {
  const player = getPlayerState(playerID, G);
  return (
    G.turn.phase === 'normal' &&
    playerID === G.turn.currentPlayerID &&
    player.health > DISCARD_MOVE_HP_COST &&
    player.hand.length >= 1
  );
}

/**
 * Find living players which verify the who constraint.
 */
export function playersInWho(who: Who, G: MyGameState): PlayerID[] {
  switch (who) {
    case 'anyone':
      return getLivingPlayerIDs(G);
    case 'oponent':
      return getOponents(G.turn.currentPlayerID, G);
    case 'self':
      return [G.turn.currentPlayerID];
  }
}

function chooseMultipleInArray<T>(inputArray: ReadonlyArray<T>, count: number, ctx: MyCtx<'mut'>): T[] {
  const array = [...inputArray];
  const chosenElements: T[] = [];
  for (let i = 0; i < count; i++) {
    if (array.length <= 0) {
      break;
    }
    const chosenIndex = ctx.random!.Die(array.length - 1);
    chosenElements.push(array.splice(chosenIndex, 1)[0]);
  }
  return chosenElements;
}

/**
 * Return random runes from hands of players selected by who.
 * If there are not enough, less are returned.
 */
export function randomHandRunes(who: Who, count: number, G: MyGameState, ctx: MyCtx): RuneParam[] {
  const possibleRunes = playersInWho(who, G).flatMap((playerID) =>
    getPlayerState(playerID, G).hand.map<RuneParam>((r) => ({ runeID: r.id, playerID }))
  );

  return chooseMultipleInArray(possibleRunes, count, ctx);
}

/**
 * Return random runes from reserves of players selected by who.
 * If there are not enough, less are returned.
 */
export function randomReserveRunes(who: Who, count: number, G: MyGameState, ctx: MyCtx): RuneParam[] {
  const possibleRunes = playersInWho(who, G).flatMap((playerID) =>
    getPlayerState(playerID, G).reserve.map<RuneParam>((r) => ({ runeID: r.id, playerID }))
  );

  return chooseMultipleInArray(possibleRunes, count, ctx);
}

/**
 * Get ID of the player in the current context (ID of the client, ID of the player playing the move)
 */
export function getContextPlayer(G: MyGameState, ctx: MyCtx): PlayerID {
  // When not in multiplayer, we consider the current player to be the context player.
  return ctx.playerID ?? G.turn.currentPlayerID;
}
