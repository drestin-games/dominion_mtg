import { PlayerID } from 'boardgame.io';
import { endNormalRound } from '../action/turn-events';
import { MyCtx } from '../Game';
import { ALL_RUNE_TYPES, RuneType, createRune } from '../Rune';
import { ALL_SPELLS } from '../spell/Spell';
import { getAllPlayers } from './getters';
import { MyGameState, PlayerState, Reserve } from './State';

export const STARTING_HEALTH = 20;
export const STARTING_SHUFFLE_USES = 1;
export const DEFAULT_CASTS_PER_TURN = 3;

export const GIFTS_COUNT = [0];
// export const GIFTS_COUNT = [4, 3, 2, 1, 0];

const INIT_RESERVE_PARAMS = {
  [RuneType.NEUTRAL]: 0,
  [RuneType.FIRE]: 3,
  [RuneType.TIME]: 3,
  [RuneType.SHADOW]: 3,
  [RuneType.GROWTH]: 3,
  [RuneType.CURSE]: 0,
};

function initReserve(param: Partial<Record<RuneType, number>>, G: MyGameState<'mut'>): Reserve<'mut'> {
  let reserve: Reserve<'mut'> = [];

  for (const runeType of ALL_RUNE_TYPES) {
    const amount = param[runeType] ?? 0;
    if (amount >= 1) {
      reserve = reserve.concat(Array.from({ length: amount }, () => createRune(runeType, G)));
    }
  }

  return reserve;
}

export function initState(ctx: MyCtx<'mut'>): MyGameState {
  const playerIDs = ctx.playOrder;
  const players: Record<PlayerID, PlayerState<'mut'>> = {};

  for (const playerID in playerIDs) {
    players[playerID] = {
      health: STARTING_HEALTH,
      hand: [],
      reserve: [], // initialized at the end of the initState()
      passed: false,
      shuffleUses: STARTING_SHUFFLE_USES,
      enchantments: [],
      castedSpells: [],
      maxCasts: DEFAULT_CASTS_PER_TURN,
    };
  }

  const G: MyGameState<'mut'> = {
    players,
    availableSpells: ALL_SPELLS.map(({ id }) => id),
    turn: {
      phase: 'normal',
      currentPlayerID: playerIDs[0],
    },
    roundIndex: -1,
    _internal: { nextEnchantmentID: 0, nextRuneID: 0 },
  };
  for (const { playerState } of getAllPlayers<'mut'>(G)) {
    playerState.reserve = initReserve(INIT_RESERVE_PARAMS, G);
  }
  endNormalRound(G, ctx);
  return G;
}
