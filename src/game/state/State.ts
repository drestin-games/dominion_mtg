import { PlayerID } from 'boardgame.io';
import { Access, Mutability } from '../utils';
import { EndState } from '../EndState';
import { Rune, RuneID } from '../Rune';
import { Enchantment, EnchantmentID } from '../spell/Enchantment';
import { BasicRunesParamType } from '../spell/ParamDef';
import { SpellID } from '../spell/Spell';

type ReserveMut = Rune[];
export type Reserve<M extends Mutability = 'const'> = Access<ReserveMut, M>;

type HandMut = Rune[];
export type Hand<M extends Mutability = 'const'> = Access<HandMut, M>;

interface PlayerStateMut {
  health: number;
  hand: Hand<'mut'>;
  reserve: Reserve<'mut'>;
  passed: boolean;
  shuffleUses: number;
  enchantments: Enchantment<'mut'>[];
  castedSpells: SpellID[];
  maxCasts: number;
}

export type PlayerState<M extends Mutability = 'const'> = Access<PlayerStateMut, M>;

type Turn =
  | {
      phase: 'runeGifts';
      currentPlayerID: PlayerID;
      giftedPlayers: PlayerID[];
      paramDef: BasicRunesParamType;
    }
  | {
      phase: 'normal';
      currentPlayerID: PlayerID;
    };
interface MyGameStateMut {
  players: Record<PlayerID, PlayerStateMut>;
  availableSpells: SpellID[];
  turn: Turn;
  gameover?: EndState;
  roundIndex: number;
  _internal: {
    nextRuneID: RuneID;
    nextEnchantmentID: EnchantmentID;
  };
}

export type MyGameState<M extends Mutability = 'const'> = Access<MyGameStateMut, M>;
