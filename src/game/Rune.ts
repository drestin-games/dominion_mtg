import { MyGameState } from './state/State';

export enum RuneType {
  NEUTRAL = 'N',
  FIRE = 'F',
  TIME = 'T',
  GROWTH = 'G',
  SHADOW = 'S',

  JOKER = 'J',
  CURSE = 'C',
}

export const BASIC_RUNE_TYPES: readonly RuneType[] = [RuneType.FIRE, RuneType.TIME, RuneType.GROWTH, RuneType.SHADOW];

export const ALL_RUNE_TYPES: readonly RuneType[] = [
  RuneType.NEUTRAL,
  ...BASIC_RUNE_TYPES,
  RuneType.CURSE,
  RuneType.JOKER,
];

export type RuneID = number;
export interface Rune {
  readonly type: RuneType;
  readonly id: RuneID;
  timesDiscarded: number;
}
export function createRune(type: RuneType, G: MyGameState<'mut'>): Rune {
  return {
    type,
    id: G._internal.nextRuneID++,
    timesDiscarded: 0,
  };
}
