import { Immutable, deepCopy } from '../utils';
import { ALL_RUNE_TYPES, Rune, RuneType } from '../Rune';

export const ANY_RUNE = 'any';
// OR array
export type SimpleCostRune = { type: 'rune'; rune: RuneType } | { type: typeof ANY_RUNE };
type OrCostRune = { type: 'or'; sub1: SimpleCostRune; sub2: SimpleCostRune };
export type CostRune = SimpleCostRune | OrCostRune;

export type CostItem = { costRune: CostRune; count: number | 'X' };

export type UnorderedCost = CostItem[];
export type Cost = Immutable<UnorderedCost>;

/**
 * Lowest is to filled first
 */
function runefillPriority(costRune: CostRune): number {
  switch (costRune.type) {
    case 'rune':
      return 1;
    case 'any':
      return 5;
    case 'or':
      return runefillPriority(costRune.sub1) + runefillPriority(costRune.sub2);
  }
}

/**
 * Lowest is to filled first
 */
function fillPriority({ count, costRune }: CostItem): number {
  return runefillPriority(costRune) * (count === 'X' ? 1000 : 1);
}

function runeCount(cost: Cost): { min: number; max?: number } {
  let min = 0;
  let hasX = false;
  for (const { count } of cost) {
    if (count === 'X') {
      hasX = true;
    } else {
      min += count;
    }
  }
  return { min, max: hasX ? undefined : min };
}

function runeCanPayRune(costRune: CostRune, rune: RuneType): boolean {
  switch (costRune.type) {
    case 'rune':
      return costRune.rune === rune;
    case 'any':
      return rune !== RuneType.CURSE;
    case 'or':
      return runeCanPayRune(costRune.sub1, rune) || runeCanPayRune(costRune.sub2, rune);
  }
}

/**
 * Apply the payment of a rune on an unordered cost (decreasing it)
 * @param cost The payment on which to apply the payment (modified by this function)
 * @param rune The rune to apply the payment from.
 * @returns If the payment could have been applied. False means the rune doesn't fit in the cost.
 */
function applyRunePayment(cost: UnorderedCost, rune: RuneType): boolean | 'X' {
  for (const costItem of cost) {
    if (runeCanPayRune(costItem.costRune, rune)) {
      if (costItem.count === 'X') {
        // just absorb the cost
        return 'X';
      } else if (costItem.count >= 1) {
        costItem.count -= 1;
        return true;
      }
    }
  }
  return false;
}

function hasX(cost: UnorderedCost): boolean {
  return cost.find((item) => item.count === 'X') !== undefined;
}

function calculateRunesCostCompatibility(cost: UnorderedCost, runes: Rune[]): { canPay: boolean; xValue?: number } {
  let jokerCount = 0;
  let X = hasX(cost) ? 0 : undefined;
  // apply the payment of each rune, reject if one can't fit
  for (const rune of runes) {
    if (rune.type === RuneType.JOKER) {
      jokerCount += 1;
      continue;
    }
    const fit = applyRunePayment(cost, rune.type);
    if (!fit) {
      return { canPay: false };
    }

    if (X !== undefined && fit === 'X') {
      X += 1;
    }
  }

  // remaining cost to pay
  const { min, max } = runeCount(cost);
  if (!(min <= jokerCount && (max === undefined || jokerCount <= max))) {
    return { canPay: false };
  }

  const xValue = X === undefined ? undefined : X + jokerCount;
  return {
    canPay: X === undefined || X >= 1,
    xValue,
  };
}

export function getXValue(cost: Cost, runes: Rune[]): number {
  return calculateRunesCostCompatibility(deepCopy<Cost>(cost) as UnorderedCost, [...runes]).xValue ?? 0;
}

export function runesCanPayCost(cost: Cost, runes: Rune[]): boolean {
  return calculateRunesCostCompatibility(deepCopy<Cost>(cost) as UnorderedCost, [...runes]).canPay;
}

/**
 * Returns if the cost can ony be paid with only one rune.
 */
export function onlyOneRuneCost(cost: Cost): boolean {
  const { min, max } = runeCount(cost);
  return min === 1 && max === 1;
}

type RuneAndAny = RuneType | typeof ANY_RUNE;
export type ExtendedRuneString = RuneAndAny | `${RuneAndAny}|${RuneAndAny}`;

function costRuneFromRuneAndAny(key: RuneAndAny): SimpleCostRune {
  if (key === ANY_RUNE) {
    return { type: 'any' };
  } else if (ALL_RUNE_TYPES.includes(key)) {
    return { type: 'rune', rune: key };
  } else {
    throw new Error(`Can't parse key to build simple spell cost: ${key}`);
  }
}

export function costRuneFromExtendedKey(key: ExtendedRuneString): CostRune {
  if (key.includes('|')) {
    const [subKey1, subKey2] = key.split('|');
    return {
      type: 'or',
      sub1: costRuneFromRuneAndAny(subKey1 as RuneAndAny),
      sub2: costRuneFromRuneAndAny(subKey2 as RuneAndAny),
    };
  } else {
    return costRuneFromRuneAndAny(key as RuneAndAny);
  }
}

export type UnorderedCostParam = Partial<Record<ExtendedRuneString, number>> & { X?: ExtendedRuneString };
export function buildUnorderedCost(input: UnorderedCostParam): Cost {
  const cost = Object.entries(input).map(([key, value]) =>
    key === 'X'
      ? {
          costRune: costRuneFromExtendedKey(value as ExtendedRuneString),
          count: 'X' as const,
        }
      : {
          costRune: costRuneFromExtendedKey(key as ExtendedRuneString),
          count: value as number,
        }
  );
  // It should be ordered  from the most specific cost to the least.
  // This should ensure that any costs are filled with the remaining runes.
  return cost.sort((a, b) => fillPriority(a) - fillPriority(b));
}
