import { PlayerID } from 'boardgame.io';
import { IsInt, IsString } from 'class-validator';
import { Immutable } from '../utils';
import { RuneID, RuneType } from '../Rune';

// eslint-disable-next-line @typescript-eslint/ban-types
type _ParamDef<Type extends string, Data extends Record<string, unknown> = {}> = {
  type: Type;
} & Data;
export type PlayerParamType = _ParamDef<'player', { oponentOnly: boolean }>;
export type BasicRunesParamType = _ParamDef<'basic-runes', { count: number; distinctOnly: boolean }>;
export type HandRunesParamType = _ParamDef<'hand-runes', { who: Who; count: number }>;
export type ReserveRunesParamType = _ParamDef<'reserve-runes', { who: Who; count: number }>;

export type ParamDef = PlayerParamType | BasicRunesParamType | HandRunesParamType | ReserveRunesParamType;

export type ParamDefObject = Immutable<{ [name: string]: ParamDef }>;

/**
 * Type: RuneID
 */
export function basicRunesParam(count: number, distinctOnly = false): BasicRunesParamType {
  return {
    type: 'basic-runes',
    count,
    distinctOnly,
  };
}

/**
 * Type: PlayerID
 */
export function oponentParam(): PlayerParamType {
  return {
    type: 'player',
    oponentOnly: true,
  };
}

export class RuneParam {
  @IsString()
  playerID!: string;

  @IsInt()
  runeID!: RuneID;
}
interface GroupedRuneParams {
  playerID: PlayerID;
  runeIDs: RuneID[];
}
export function groupByPlayer(params: readonly RuneParam[]): GroupedRuneParams[] {
  const groups: Map<PlayerID, RuneID[]> = new Map();
  for (const { playerID, runeID } of params) {
    if (groups.has(playerID)) {
      groups.get(playerID)!.push(runeID);
    } else {
      groups.set(playerID, [runeID]);
    }
  }
  return Array.from(groups.entries()).map(([playerID, runeIDs]) => ({
    playerID,
    runeIDs,
  }));
}

export type Who = 'anyone' | 'self' | 'oponent';
/**
 * Type: HandRuneParam[]
 */
export function handRunesParam(who: Who, count: number): HandRunesParamType {
  return {
    type: 'hand-runes',
    who,
    count,
  };
}

export function reserveRunesParam(who: Who, count: number): ReserveRunesParamType {
  return {
    type: 'reserve-runes',
    who,
    count,
  };
}

type ParamType<P extends ParamDef> = {
  player: PlayerID;
  'basic-runes': RuneType[];
  'hand-runes': RuneParam[];
  'reserve-runes': RuneParam[];
}[P['type']];

export type CastParamsParams<P extends ParamDefObject> = {
  [Name in keyof P]: ParamType<P[Name]>;
};
