/* eslint-disable @typescript-eslint/ban-types */
import { PlayerID } from 'boardgame.io';
import { Access, Mutability } from '../utils';
import { addLog, enchantmentTick, loseHealth } from '../action/base-actions';
import { endTurn } from '../action/turn-events';
import { MyCtx } from '../Game';
import { getPlayerState, isDead } from '../state/getters';
import { MyGameState } from '../state/State';

type EnchantmentDuration =
  | 'infinite'
  | {
      [K in TickType]?: number;
    };
export type TickType = 'rounds' | 'turns' | 'triggers';

export type EnchantmentID = number;

interface TriggerFunctionParams<P extends Record<string, unknown>> {
  readonly playerID: PlayerID;
  readonly G: MyGameState<'mut'>;
  readonly ctx: MyCtx<'mut'>;
  readonly stacks: number;
  /** Can be mutable */
  readonly params: P;
}
type TriggerFunction<P extends Record<string, unknown> = {}> = (params: TriggerFunctionParams<P>) => void;

interface _Enchantment extends EnchantmentTriggers {
  id: EnchantmentID;
  name: EnchantmentName;
  duration: EnchantmentDuration;
  stackable: boolean;
  stacks: number;
  turnCreated: PlayerID;
}

interface EnchantmentTriggers {
  /** value = damage to suffer */
  onBeforeSufferDamage?: TriggerFunction<{ damage: number }>;
  onLifeLost?: TriggerFunction<{ readonly lifeLost: number }>;
  onBeginRound?: TriggerFunction;
}
export type Enchantment<M extends Mutability = 'const'> = Access<_Enchantment, M>;

interface EnchantmentParams extends EnchantmentTriggers {
  name: EnchantmentName;
  duration: EnchantmentDuration;
  /** Default false. It needs to be taken into account in the effect */
  stackable?: boolean;
  /** Default 1 */
  stacks?: number;
}

export function createEnchantment(G: MyGameState<'mut'>, ctx: MyCtx, params: EnchantmentParams): Enchantment<'mut'> {
  return {
    id: G._internal.nextEnchantmentID++,
    ...params,
    stackable: params.stackable ?? false,
    stacks: params.stacks ?? 1,
    turnCreated: G.turn.currentPlayerID,
  };
}

type TriggerName = keyof EnchantmentTriggers;
type TriggerParams<T extends TriggerName> = Parameters<Required<EnchantmentTriggers>[T]>[0];
type TriggerReturn<T extends TriggerName> = TriggerParams<T>['params'];

export function trigger<T extends TriggerName>(
  triggerName: T,
  params: Omit<TriggerParams<T>, 'stacks'>
): TriggerReturn<T> {
  for (const enchantment of getPlayerState<'mut'>(params.playerID, params.G).enchantments) {
    if (!enchantment[triggerName]) {
      continue;
    }

    addLog(params.ctx, { type: 'trigger-enchantment', playerID: params.playerID, enchantment });
    params.ctx.effects?.triggeredEnchantment({ playerID: params.playerID });

    // Failed to type this line due to missing generics reduction in functions.
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    enchantment[triggerName]!({ ...params, stacks: enchantment.stacks } as any);

    enchantmentTick(params.playerID, enchantment, 'triggers', params.G, params.ctx);
  }
  if (isDead(params.G.turn.currentPlayerID, params.G)) {
    endTurn(params.G, params.ctx);
  }
  return params.params;
}

export type EnchantmentName = 'vulnerable' | 'decay';
export function vulnerableEnchantment(G: MyGameState<'mut'>, ctx: MyCtx, stacks = 1): Enchantment<'mut'> {
  return createEnchantment(G, ctx, {
    duration: 'infinite',
    name: 'vulnerable',
    stackable: true,
    stacks,
    onBeforeSufferDamage({ params, stacks }) {
      params.damage += stacks;
    },
  });
}

export function decayEnchantment(G: MyGameState<'mut'>, ctx: MyCtx, stacks = 1): Enchantment<'mut'> {
  return createEnchantment(G, ctx, {
    duration: 'infinite',
    name: 'decay',
    stackable: true,
    stacks,
    onBeginRound({ G, ctx, playerID, stacks }) {
      loseHealth(playerID, stacks, G, ctx);
    },
  });
}
