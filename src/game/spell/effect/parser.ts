/* eslint-disable @typescript-eslint/no-explicit-any */
import { ALL_RUNE_TYPES, RuneType } from '../../Rune';
import { CastConditions } from '../CastConditions';
import { ExtendedRuneString, UnorderedCost, costRuneFromExtendedKey } from '../Cost';
import { Who } from '../ParamDef';
import { FilledEffect } from './buildSpell';
import {
  ALL_EFFECT_PARAM_FILLERS,
  EffectParamFiller,
  chosenHandRunesEffectParam,
  chosenNewRunesEffectParam,
  chosenReserveRunesEffectParam,
  literalNumberEffectParam,
  literalRuneTypesEffectParam,
  randomHandRunesEffectParam,
  randomNewRunesEffectParam,
  randomReserveRunesEffectParam,
  xEffectParam,
} from './EffectParamFiller';
import { ALL_SPELL_EFFECTS } from './SpellEffect';

const EFFECT_SEPARATOR = ';';
const EFFECT_TYPE_SEPARATOR = ':';
const EFFECT_PARAM_SEPARATOR = ',';
const CONDITIONS_SEPARATOR = '!';
const COST_ITEMS_SEPARATOR = ',';

export function parseEffectString(
  str: string
): { castConditions?: CastConditions | undefined; filledEffects: FilledEffect[] } {
  if (str.includes(CONDITIONS_SEPARATOR)) {
    const [condStr, effectsStr] = str.split(CONDITIONS_SEPARATOR);
    return {
      castConditions: parseConditions(condStr),
      filledEffects: parseAllEffects(effectsStr),
    };
  } else {
    return { filledEffects: parseAllEffects(str) };
  }
}

function parseAllEffects(str: string): FilledEffect[] {
  return str.split(EFFECT_SEPARATOR).map((s) => parseOneEffect(s));
}

function parseOneEffect(str: string): FilledEffect {
  const [id, fillerIDs] = str.split(EFFECT_TYPE_SEPARATOR);

  const effect = ALL_SPELL_EFFECTS.find((e) => e.id === id);
  if (!effect) {
    throw new Error(`Unknown effect id: ${id}`);
  }

  const fillers = fillerIDs.split(EFFECT_PARAM_SEPARATOR).map((s) => parseParamFiller(s));

  if (effect.paramsDef.length !== fillers.length) {
    throw new Error(`Wrong number of param filler for effect ${effect.id}`);
  }
  for (let i = 0; i < effect.paramsDef.length; i++) {
    if (effect.paramsDef[i].type !== fillers[i].type) {
      throw new Error(`Wrong type of param filler at pos ${i} of effect ${effect.id}`);
    }
  }

  return {
    effect,
    fillers,
  } as FilledEffect;
}

const who = '(anyone|oponent|self)';
const HAND_RUNES_REGEX = `^(@|\\?)handRunes\\(${who}\\|(\\d)\\)$`;
const RESERVE_RUNES_REGEX = `^(@|\\?)reserveRunes\\(${who}\\|(\\d)\\)$`;
const CHOSEN_NEW_RUNES_REGEX = /^(@|\?)newRunes\((\d)\)$/;
const X_REGEX = /^(\d?)X((\/\d)?)$/;
function parseParamFiller(str: string): EffectParamFiller<any> {
  const n = parseInt(str);
  if (!isNaN(n) && n.toString() === str) {
    return literalNumberEffectParam(n);
  }

  const handRuneMatches = str.match(HAND_RUNES_REGEX);
  if (handRuneMatches) {
    const random = handRuneMatches[1] === '?';
    const who = handRuneMatches[2] as Who;
    const count = parseInt(handRuneMatches[3]);
    return random ? randomHandRunesEffectParam(who, count) : chosenHandRunesEffectParam(who, count);
  }
  const reserveRuneMatches = str.match(RESERVE_RUNES_REGEX);
  if (reserveRuneMatches) {
    const random = reserveRuneMatches[1] === '?';
    const who = reserveRuneMatches[2] as Who;
    const count = parseInt(reserveRuneMatches[3]);
    return random ? randomReserveRunesEffectParam(who, count) : chosenReserveRunesEffectParam(who, count);
  }
  const newRuneMatches = str.match(CHOSEN_NEW_RUNES_REGEX);
  if (newRuneMatches) {
    const random = newRuneMatches[1] === '?';
    const count = parseInt(newRuneMatches[2]);
    return random ? randomNewRunesEffectParam(count) : chosenNewRunesEffectParam(count);
  }
  if (ALL_EFFECT_PARAM_FILLERS[str]) {
    return ALL_EFFECT_PARAM_FILLERS[str]();
  }
  if (str.split('-').every((runeType) => ALL_RUNE_TYPES.includes(runeType as RuneType))) {
    return literalRuneTypesEffectParam(str.split('-') as RuneType[]);
  }
  const xMatches = str.match(X_REGEX);
  if (xMatches) {
    const factorStr = xMatches[1];
    const factor = factorStr === '' ? 1 : parseInt(factorStr);
    const divStr = xMatches[2];
    const div = divStr === '' ? 1 : parseInt(divStr.substring(1));
    return xEffectParam(factor, div);
  }

  throw new Error(`Couldn't parse param filler: ${str}`);
}

function parseConditions(str: string): CastConditions {
  const [maxCountPerRound, minIndexInRound, maxIndexInRound] = str
    .split(EFFECT_PARAM_SEPARATOR)
    .map((s) => (s === '' ? undefined : parseInt(s)));
  return {
    maxCountPerRound,
    minIndexInRound,
    maxIndexInRound,
  };
}

const COST_ITEM_REGEX = `^(\\d+|X?)([a-zA-WYZ|]+)$`;
export function parseCost(str: string): UnorderedCost {
  return str.split(COST_ITEMS_SEPARATOR).map((costStr) => {
    const matches = costStr.match(COST_ITEM_REGEX);
    if (!matches) {
      throw new Error(`Couldn't parse cost string ${costStr}`);
    }

    const countStr = matches[1];
    const count = countStr === '' ? 1 : countStr === 'X' ? 'X' : parseInt(countStr);
    const costRune = costRuneFromExtendedKey(matches[2] as ExtendedRuneString);

    return { costRune, count };
  });
}
