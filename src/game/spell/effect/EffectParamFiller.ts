import { BASIC_RUNE_TYPES, RuneType, createRune } from '../../Rune';
import { getPlayerState, randomHandRunes, randomReserveRunes } from '../../state/getters';
import { Immutable } from '../../utils';
import { getXValue } from '../Cost';
import { ParamDefObject, Who, basicRunesParam, handRunesParam, oponentParam, reserveRunesParam } from '../ParamDef';
import { CastParams, getSpell } from '../Spell';
import { EffectParamType, EffectParamTypeID } from './SpellEffect';

export interface EffectParamFiller<T extends EffectParamTypeID> {
  type: T;
  frDescription: string;
  updateSpellParamsDef?: () => ParamDefObject;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getValue: (params: CastParams<any>) => EffectParamType<T>;
}
export interface EffectParamFillerParams<T extends EffectParamTypeID, P extends ParamDefObject> {
  type: T;
  frDescription: string;
  updateSpellParamsDef?: () => P;
  getValue: (params: CastParams<P>) => EffectParamType<T>;
}
// FIXME better param id generation
let nextParamID = 0;
function generateParamID(): string {
  return `__param-${nextParamID++}`;
}
function createEffectParamFiller<T extends EffectParamTypeID, P extends ParamDefObject>(
  params: EffectParamFillerParams<T, P>
): EffectParamFiller<T> {
  return params;
}

// \d
export function literalNumberEffectParam(n: number): EffectParamFiller<'natural'> {
  return createEffectParamFiller({
    type: 'natural',
    frDescription: n.toString(),
    getValue: () => n,
  });
}

// @oponent
export function targetOponentEffectParam(): EffectParamFiller<'player'> {
  const paramID = generateParamID();
  return createEffectParamFiller({
    type: 'player',
    frDescription: "l'adversaire ciblé",
    updateSpellParamsDef: () => ({ [paramID]: oponentParam() }),
    getValue: ({ params }) => params[paramID],
  });
}

// caster
export function casterEffectParam(): EffectParamFiller<'player'> {
  return createEffectParamFiller({
    type: 'player',
    frDescription: 'le lanceur',
    getValue: ({ G }) => G.turn.currentPlayerID,
  });
}

// @newRunes(1)
export function chosenNewRunesEffectParam(count: number): EffectParamFiller<'new-runes'> {
  const paramID = generateParamID();
  return createEffectParamFiller({
    type: 'new-runes',
    frDescription: `${count} Runes au choix`,
    updateSpellParamsDef: () => ({ [paramID]: basicRunesParam(count) }),
    getValue: ({ G, params }) => params[paramID].map((type) => createRune(type, G)),
  });
}

// ?newRunes(1)
export function randomNewRunesEffectParam(count: number): EffectParamFiller<'new-runes'> {
  return createEffectParamFiller({
    type: 'new-runes',
    frDescription: `${count} Runes aléatoires`,
    getValue: ({ G, ctx }) => {
      return Array.from({ length: count }, () => createRune(BASIC_RUNE_TYPES[ctx.random!.D4() - 1], G));
    },
  });
}

// @handRunes(oponent|1)
export function chosenHandRunesEffectParam(who: Who, count: number): EffectParamFiller<'hand-runes'> {
  const paramID = generateParamID();
  return createEffectParamFiller({
    type: 'hand-runes',
    frDescription: `${count} Runes au choix de la main de ${frWho(who)}`,
    updateSpellParamsDef: () => ({ [paramID]: handRunesParam(who, count) }),
    getValue: ({ params }) => params[paramID],
  });
}

// ?handRunes(oponent|1)
export function randomHandRunesEffectParam(who: Who, count: number): EffectParamFiller<'hand-runes'> {
  return createEffectParamFiller({
    type: 'hand-runes',
    frDescription: `${count} Runes aléatoires de la main de ${frWho(who)}`,
    getValue: ({ G, ctx }) => randomHandRunes(who, count, G, ctx),
  });
}

// @reserveRunes(oponent|1)
export function chosenReserveRunesEffectParam(who: Who, count: number): EffectParamFiller<'reserve-runes'> {
  const paramID = generateParamID();
  return createEffectParamFiller({
    type: 'reserve-runes',
    frDescription: `${count} Runes de la réserve de ${frWho(who)}`,
    updateSpellParamsDef: () => ({ [paramID]: reserveRunesParam(who, count) }),
    getValue: ({ params }) => params[paramID],
  });
}

// ?handRunes(oponent|1)
export function randomReserveRunesEffectParam(who: Who, count: number): EffectParamFiller<'reserve-runes'> {
  return createEffectParamFiller({
    type: 'reserve-runes',
    frDescription: `${count} Runes aléatoires de la réserve de ${frWho(who)}`,
    getValue: ({ G, ctx }) => randomReserveRunes(who, count, G, ctx),
  });
}

// J-J
export function literalRuneTypesEffectParam(runeTypes: RuneType[]): EffectParamFiller<'new-runes'> {
  return createEffectParamFiller({
    type: 'new-runes',
    frDescription: `##${runeTypes.join('##')}##`,
    getValue: ({ G }) => runeTypes.map((type) => createRune(type, G)),
  });
}

// castedCount
export function castedCountEffectParam(): EffectParamFiller<'natural'> {
  return createEffectParamFiller({
    type: 'natural',
    frDescription: 'le nombre de sorts précédemment lancés de',
    getValue: ({ G }) => getPlayerState(G.turn.currentPlayerID, G).castedSpells.length,
  });
}

// 2X
export function xEffectParam(mult: number, div: number): EffectParamFiller<'natural'> {
  return createEffectParamFiller({
    type: 'natural',
    frDescription: `${mult === 1 ? '' : mult}X${div === 1 ? '' : `/${div}`}`,
    getValue: ({ usedRunes, spellID }) => {
      return Math.floor((mult * getXValue(getSpell(spellID).cost, usedRunes)) / div);
    },
  });
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const ALL_EFFECT_PARAM_FILLERS: Immutable<Record<string, () => EffectParamFiller<any>>> = {
  '@oponent': targetOponentEffectParam,
  caster: casterEffectParam,
  castedCount: castedCountEffectParam,
};

function frWho(who: Who): string {
  switch (who) {
    case 'anyone':
      return "n'importe qui";
    case 'oponent':
      return 'adversaires';
    case 'self':
      return 'le lanceur';
  }
}
