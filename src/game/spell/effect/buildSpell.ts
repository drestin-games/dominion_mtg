import { UnorderedCostParam, buildUnorderedCost } from '../Cost';
import { Spell, SpellID, createSpell } from '../Spell';
import { EffectParamFiller } from './EffectParamFiller';
import { parseCost, parseEffectString } from './parser';
import { EffectParamDef, EffectParamsDefList, SpellEffect } from './SpellEffect';

export type EffectFillerList<P extends EffectParamsDefList> = P extends readonly []
  ? []
  : P extends readonly [EffectParamDef]
  ? [EffectParamFiller<P[0]['type']>]
  : P extends readonly [EffectParamDef, EffectParamDef]
  ? [EffectParamFiller<P[0]['type']>, EffectParamFiller<P[1]['type']>]
  : P extends readonly [EffectParamDef, EffectParamDef, EffectParamDef]
  ? [EffectParamFiller<P[0]['type']>, EffectParamFiller<P[1]['type']>, EffectParamFiller<P[2]['type']>]
  : never;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type FilledEffect<P extends EffectParamsDefList = any> = {
  effect: SpellEffect<P>;
  fillers: EffectFillerList<P>;
};

export function fillEffect<P extends EffectParamsDefList>(
  effect: SpellEffect<P>,
  fillers: EffectFillerList<P>
): FilledEffect<P> {
  return { effect, fillers };
}

interface BuildSpellFromEffectsParams {
  cost: string | UnorderedCostParam;
  id: SpellID;
  frTitle: string;
  effectsString: string;
}

export function buildSpellFromEffects(buildEffectParams: BuildSpellFromEffectsParams): Spell {
  let paramsDef: Spell['paramsDef'] = {};

  const { castConditions, filledEffects } = parseEffectString(buildEffectParams.effectsString);

  for (const filler of filledEffects.flatMap((e) => e.fillers)) {
    if (filler.updateSpellParamsDef) {
      paramsDef = { ...paramsDef, ...filler.updateSpellParamsDef() };
    }
  }

  const effectFunction: Spell['effect'] = (castParams) => {
    for (const { effect, fillers } of filledEffects) {
      const effectParams: unknown[] = [];
      for (const filler of fillers) {
        effectParams.push(filler.getValue(castParams));
      }
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      effect.execute(castParams.G, castParams.ctx, effectParams as any);
    }
  };

  const frDescription = filledEffects
    .map(({ effect, fillers }) => effect.frDescription(fillers))
    .join('\n')
    .replace(' à le ', ' au ')
    .replace(' à les ', ' aux ')
    .replace(' de le ', ' du ')
    .replace(' de les ', ' des ')
    .replace(' de a', " d'a");

  const endDescription = frDescription[0].toUpperCase() + frDescription.substring(1);

  const cost =
    typeof buildEffectParams.cost === 'string'
      ? parseCost(buildEffectParams.cost)
      : buildUnorderedCost(buildEffectParams.cost);

  return createSpell({
    id: buildEffectParams.id,
    frTitle: buildEffectParams.frTitle,
    cost,
    effect: effectFunction,
    paramsDef,
    frDescription: endDescription,
    castConditions,
  });
}
