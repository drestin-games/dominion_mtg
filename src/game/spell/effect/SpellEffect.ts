import { PlayerID } from 'boardgame.io';
import { ReactNode } from 'react';
import { Immutable } from '../../utils';
import {
  addEnchantment,
  damage,
  destroyRunesFromHand,
  destroyRunesFromReserve,
  discardRunes,
  gainCasts,
  gainRunes,
  gainShuffleUses,
  heal,
  loseHealth,
} from '../../action/base-actions';
import { MyCtx } from '../../Game';
import { Rune } from '../../Rune';
import { MyGameState } from '../../state/State';
import { decayEnchantment, vulnerableEnchantment } from '../Enchantment';
import { RuneParam, groupByPlayer } from '../ParamDef';
import { EffectFillerList } from './buildSpell';

export type EffectParamsDefList =
  | readonly []
  | readonly [EffectParamDef]
  | readonly [EffectParamDef, EffectParamDef]
  | readonly [EffectParamDef, EffectParamDef, EffectParamDef];

type EffectExecuteParams<P extends EffectParamsDefList> = P extends readonly []
  ? []
  : P extends readonly [EffectParamDef]
  ? [EffectParamType<P[0]['type']>]
  : P extends readonly [EffectParamDef, EffectParamDef]
  ? [EffectParamType<P[0]['type']>, EffectParamType<P[1]['type']>]
  : P extends readonly [EffectParamDef, EffectParamDef, EffectParamDef]
  ? [EffectParamType<P[0]['type']>, EffectParamType<P[1]['type']>, EffectParamType<P[2]['type']>]
  : never;

export type EffectParamType<T extends EffectParamTypeID> = {
  natural: number;
  player: PlayerID;
  'new-runes': readonly Rune[];
  'hand-runes': readonly RuneParam[];
  'reserve-runes': readonly RuneParam[];
}[T];

export interface SpellEffect<P extends EffectParamsDefList = EffectParamsDefList> {
  id: string;
  paramsDef: P;
  execute: (G: MyGameState<'mut'>, ctx: MyCtx<'mut'>, params: EffectExecuteParams<P>) => void;
  frDescription: (fillers: EffectFillerList<P>) => ReactNode;
}

const amountParam = {
  type: 'natural',
} as const;
const playerParam = {
  type: 'player',
} as const;
const newRunesParam = {
  type: 'new-runes',
} as const;
const handRunesParam = {
  type: 'hand-runes',
} as const;
const reserveRunesParam = {
  type: 'reserve-runes',
} as const;

export type EffectParamDef =
  | typeof amountParam
  | typeof playerParam
  | typeof newRunesParam
  | typeof handRunesParam
  | typeof reserveRunesParam;
export type EffectParamTypeID = EffectParamDef['type'];

const allSpellEffects: SpellEffect[] = [];

type CreateEffectParams<P extends EffectParamsDefList> = SpellEffect<P>;
function createEffect<P extends EffectParamsDefList>(params: CreateEffectParams<P>): SpellEffect<P> {
  if (allSpellEffects.find((e) => e.id === params.id)) {
    throw new Error(`An effect with id ${params.id} already exists`);
  }
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  allSpellEffects.push(params as any);
  return params;
}
export const damageEffect = createEffect({
  id: 'damage',
  paramsDef: [amountParam, playerParam] as const,
  execute: (G, ctx, [amount, target]) => {
    damage(target, amount, G, ctx);
  },
  frDescription: ([damageFiller, targetFiller]) =>
    `Inflige ${damageFiller.frDescription} dégat(s) à ${targetFiller.frDescription}`,
});
export const healEffect = createEffect({
  id: 'heal',
  paramsDef: [amountParam, playerParam] as const,
  execute: (G, ctx, [amount, target]) => {
    heal(target, amount, G, ctx);
  },
  frDescription: ([damageFiller, targetFiller]) =>
    `Soigne ${damageFiller.frDescription} PV à ${targetFiller.frDescription}`,
});
export const loseHealthEffect = createEffect({
  id: 'loseHealth',
  paramsDef: [amountParam, playerParam] as const,
  execute: (G, ctx, [amount, target]) => {
    loseHealth(target, amount, G, ctx);
  },
  frDescription: ([damageFiller, targetFiller]) =>
    `${targetFiller.frDescription} perd ${damageFiller.frDescription} PV`,
});
export const gainCastsEffect = createEffect({
  id: 'gainCasts',
  paramsDef: [amountParam, playerParam] as const,
  execute: (G, ctx, [amount, playerID]) => {
    gainCasts(playerID, amount, G, ctx);
  },
  frDescription: ([amountFiller, playerIDFiller]) =>
    `${playerIDFiller.frDescription} obtient ${amountFiller.frDescription} Lancer(s)`,
});
export const gainShufflesEffect = createEffect({
  id: 'gainShuffles',
  paramsDef: [amountParam, playerParam] as const,
  execute: (G, ctx, [amount, playerID]) => {
    gainShuffleUses(playerID, amount, G, ctx);
  },
  frDescription: ([amountFiller, playerIDFiller]) =>
    `${playerIDFiller.frDescription} obtient ${amountFiller.frDescription} Mélange(s)`,
});
export const gainRunesEffect = createEffect({
  id: 'gainRunes',
  paramsDef: [newRunesParam, playerParam] as const,
  execute: (G, ctx, [newRunes, gainerID]) => {
    gainRunes(gainerID, newRunes, G, ctx);
  },
  frDescription: ([runesFiller, gainerIDFiller]) =>
    `${gainerIDFiller.frDescription} obtient ${runesFiller.frDescription}`,
});
export const discardEffect = createEffect({
  id: 'discard',
  paramsDef: [handRunesParam] as const,
  execute: (G, ctx, [runes]) => {
    for (const { playerID, runeIDs } of groupByPlayer(runes)) {
      discardRunes(playerID, runeIDs, G, ctx);
    }
  },
  frDescription: ([runesFiller]) => `Défausse ${runesFiller.frDescription}`,
});
export const destroyHandRunesEffect = createEffect({
  id: 'destroyHandRunes',
  paramsDef: [handRunesParam] as const,
  execute: (G, ctx, [runes]) => {
    for (const { playerID, runeIDs } of groupByPlayer(runes)) {
      destroyRunesFromHand(playerID, runeIDs, G, ctx);
    }
  },
  frDescription: ([runesFiller]) => `Détruit ${runesFiller.frDescription}`,
});
export const destroyReserveRunesEffect = createEffect({
  id: 'destroyReserveRunes',
  paramsDef: [reserveRunesParam] as const,
  execute: (G, ctx, [runes]) => {
    for (const { playerID, runeIDs } of groupByPlayer(runes)) {
      destroyRunesFromReserve(playerID, runeIDs, G, ctx);
    }
  },
  frDescription: ([runesFiller]) => `Détruit ${runesFiller.frDescription}`,
});
export const enchantVulnerableEffect = createEffect({
  id: 'enchantVulnerable',
  paramsDef: [amountParam, playerParam] as const,
  execute: (G, ctx, [stacks, target]) => {
    addEnchantment(target, vulnerableEnchantment(G, ctx, stacks), G, ctx);
  },
  frDescription: ([stacksFiller, targetFiller]) =>
    `${targetFiller.frDescription} obtient Vulnérable(×${stacksFiller.frDescription}): +1 dégat subit`,
});
export const enchantDecayEffect = createEffect({
  id: 'enchantDecay',
  paramsDef: [amountParam, playerParam] as const,
  execute: (G, ctx, [stacks, target]) => {
    addEnchantment(target, decayEnchantment(G, ctx, stacks), G, ctx);
  },
  frDescription: ([stacksFiller, targetFiller]) =>
    `${targetFiller.frDescription} obtient Décomposition(×${stacksFiller.frDescription}): -1PV/round`,
});

export const ALL_SPELL_EFFECTS: Immutable<SpellEffect[]> = allSpellEffects;
