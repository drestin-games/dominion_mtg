import { arrayCount } from '../utils';
import { SpellID } from './Spell';

export interface CastConditions {
  maxCountPerRound?: number;
  minIndexInRound?: number;
  maxIndexInRound?: number;
}

export function areCastConditionsSastified(
  { maxCountPerRound, minIndexInRound, maxIndexInRound }: CastConditions,
  spellID: SpellID,
  castedSpells: readonly SpellID[]
): boolean {
  return (
    !(maxCountPerRound !== undefined && arrayCount(castedSpells, (id) => id === spellID) > maxCountPerRound) &&
    !(minIndexInRound !== undefined && castedSpells.length < minIndexInRound) &&
    !(maxIndexInRound !== undefined && castedSpells.length > maxIndexInRound)
  );
}
