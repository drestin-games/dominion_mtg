import Papa from 'papaparse';
import { MyCtx } from '../Game';
import { Rune } from '../Rune';
import { MyGameState } from '../state/State';
import { CastConditions } from './CastConditions';
import { Cost } from './Cost';
import { buildSpellFromEffects } from './effect/buildSpell';
import { CastParamsParams, ParamDefObject } from './ParamDef';

export type SpellID = string;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type SpellParams = Record<string, any>;

export interface CastParams<P extends ParamDefObject> {
  G: MyGameState<'mut'>;
  ctx: MyCtx<'mut'>;
  usedRunes: Rune[];
  spellID: SpellID;
  params: CastParamsParams<P>;
}

export type Spell = {
  cost: Cost;
  id: SpellID;
  frTitle: string;
  frDescription: string;
  paramsDef: ParamDefObject;
  castConditions: CastConditions;
  effect: (params: CastParams<ParamDefObject>) => void;
};

interface CreateSpellParams<P extends ParamDefObject> {
  cost: Cost;
  id: SpellID;
  frTitle: string;
  frDescription: string;
  paramsDef: P;
  castConditions?: CastConditions;
  effect: (params: CastParams<P>) => void;
}
const allSpellsMut: Spell[] = [];

const SPELLS_CSV = `
id,frTitle,cost,effectsString
fireball,Boule de feu,2F,"damage:1,@oponent"
improvisation,Débrouillardise,6any,"gainRunes:@newRunes(1),caster"
speedup,Accélération,2G|T,"gainCasts:2,caster"
impatience,Impatience,"1F|G,1any","gainShuffles:1,caster"
distraction,Distraction,2S|T,"discard:?handRunes(oponent|2)"
charge,Charger,2F|G,"gainRunes:@newRunes(1),caster"
heal,Soin,"3S|G,1any","heal:1,caster"
joker,Joker,"3G,S,F,T","gainRunes:J-J,caster"
curse,Malédiction,3S,"gainRunes:C,@oponent"
vulnerability,Vulnérabilité,"4F|T,any","enchantVulnerable:1,@oponent"
purify,Purification,"2G|F,2any","destroyHandRunes:@handRunes(self|1)"
snowball,Boule de neige,"2T,2any","damage:castedCount,@oponent"
gather,Emmagasiner,"4G,any","gainRunes:?newRunes(3),caster"
forget,Oublier,"3S,2any","destroyHandRunes:@handRunes(oponent|1)"
decay,Décomposition,"4S,2any","enchantDecay:1,@oponent"
explosion,Explosion,"3F,XF",",,0!damage:2X,@oponent"
kamehameha,Kamehameha,"25any","damage:10,@oponent"
`.trim();
/**
 * Utility function to create spells and register them.
 * @returns a reference of the created spell.
 */
export function createSpell<P extends ParamDefObject>(params: CreateSpellParams<P>): Spell {
  if (allSpellsMut.find(({ id }) => params.id === id)) {
    throw new Error(`A spell with id ${params.id} already exists`);
  }
  const spell: Spell = {
    ...params,
    castConditions: params.castConditions ?? {},
    frDescription: params.frDescription,
  } as Spell;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  allSpellsMut.push(spell);
  return spell;
}

interface CsvSpellType {
  id: string;
  frTitle: string;
  cost: string;
  effectsString: string;
}
function parseSpellsCsv(): void {
  const parseResult = Papa.parse(SPELLS_CSV, { header: true });
  if (parseResult.errors.length > 0) {
    throw new Error(`Error while parsing SPELLS_CSV: ${JSON.stringify(parseResult.errors)}`);
  }
  parseResult.data.forEach((data) => buildSpellFromEffects(data as CsvSpellType));
}
parseSpellsCsv();

export const ALL_SPELLS: ReadonlyArray<Spell> = allSpellsMut;

export function getSpell(spellID: SpellID): Spell {
  const spell = ALL_SPELLS.find((s) => s.id === spellID);
  if (!spell) {
    throw new Error(`Unknown spell id: ${spellID}`);
  }
  return spell;
}
