import { INVALID_MOVE } from 'boardgame.io/core';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';
import { MyCtx } from '../Game';
import { BASIC_RUNE_TYPES, RuneID, RuneType, createRune } from '../Rune';
import { ParamDef, RuneParam } from '../spell/ParamDef';
import { Spell, SpellID, SpellParams, getSpell } from '../spell/Spell';
import {
  HandRange,
  canCastSpell,
  canPaySpell,
  canUseDiscardMove,
  getCurrentPlayerState,
  getHandRunes,
  getPlayerState,
  isDead,
  isRuneInHand,
  isRuneInReserve,
  isValidPlayer,
  isValidRange,
  playerExists,
  rangeToString,
} from '../state/getters';
import { MyGameState } from '../state/State';
import { arrayUnique } from '../utils';
import { addLog, discardRuneRange, discardRunes, gainRunes, loseHealth, playerShuffleHand } from './base-actions';
import { endTurn } from './turn-events';

export type MoveReturn = void | typeof INVALID_MOVE;

export interface CastSpellArgs {
  spellID: SpellID;
  range: HandRange;
  params: SpellParams;
}

export function checkOneSpellParam(paramDef: ParamDef, paramValue: unknown, G: MyGameState): boolean {
  if (paramValue === undefined) {
    return false;
  }

  if (paramDef.type === 'player') {
    return (
      typeof paramValue === 'string' &&
      playerExists(paramValue, G) &&
      !(G.turn.currentPlayerID === paramValue && paramDef.oponentOnly)
    );
  } else if (paramDef.type === 'basic-runes') {
    return (
      paramValue instanceof Array &&
      paramValue.every((v) => BASIC_RUNE_TYPES.includes(v as RuneType)) &&
      !(paramDef.distinctOnly && !arrayUnique(paramValue, (a, b) => a === b)) &&
      paramValue.length === paramDef.count
    );
  } else if (paramDef.type === 'hand-runes') {
    if (!(paramValue instanceof Array)) {
      return false;
    }
    const handRunes = plainToClass(RuneParam, paramValue);
    const errors = validateSync(handRunes);
    if (errors.length > 0) {
      return false;
    }
    return (
      arrayUnique(handRunes, (a, b) => a.runeID === b.runeID && a.playerID === b.playerID) &&
      handRunes.every(
        ({ runeID: runeIndex, playerID }) =>
          playerExists(playerID, G) && isValidPlayer(playerID, paramDef.who, G) && isRuneInHand(playerID, runeIndex, G)
      )
    );
  } else if (paramDef.type === 'reserve-runes') {
    if (!(paramValue instanceof Array)) {
      return false;
    }
    const reserveRunes = plainToClass(RuneParam, paramValue);
    const errors = validateSync(reserveRunes);
    if (errors.length > 0) {
      return false;
    }
    return (
      arrayUnique(reserveRunes, (a, b) => a.runeID === b.runeID && a.playerID === b.playerID) &&
      reserveRunes.every(
        ({ runeID: runeIndex, playerID }) =>
          playerExists(playerID, G) &&
          isValidPlayer(playerID, paramDef.who, G) &&
          isRuneInReserve(playerID, runeIndex, G)
      )
    );
  }

  console.error(`Unhandled param: ${JSON.stringify(paramDef)}`);
  return false;
}

function checkSpellParams(spell: Spell, spellParams: SpellParams, G: MyGameState): boolean {
  if (!spell.paramsDef) {
    return true;
  }

  for (const [paramName, paramDef] of Object.entries(spell.paramsDef)) {
    const value = spellParams[paramName];
    if (!checkOneSpellParam(paramDef, value, G)) {
      console.error(`Invalid value for spell param ${paramName} (${paramDef.type}): ${JSON.stringify(value)}`);
      return false;
    }
  }

  return true;
}

export function castSpell(
  G: MyGameState<'mut'>,
  ctx: MyCtx<'mut'>,
  { spellID, range, params }: CastSpellArgs
): MoveReturn {
  if (!G.availableSpells.includes(spellID)) {
    console.error(`Spell ${spellID} is not available`);
    return INVALID_MOVE;
  }
  const playerID = G.turn.currentPlayerID;
  const player = getPlayerState<'mut'>(playerID, G);
  const spell = getSpell(spellID);

  if (!canCastSpell(playerID, spell, G)) {
    console.error(`Player ${playerID} can't cast spell ${spell.id}: conditions not satisfied`);
    return INVALID_MOVE;
  }
  if (player.castedSpells.length >= player.maxCasts) {
    console.error(`Player ${playerID} tries to cast too many spells in this round`);
    return INVALID_MOVE;
  }
  if (!isValidRange(playerID, range, G)) {
    console.error(`Range ${rangeToString(range)} is invalid for player ${playerID}`);
    return INVALID_MOVE;
  }
  const usedRunes = getHandRunes(playerID, range, G);
  if (!canPaySpell(G.turn.currentPlayerID, spellID, range, G)) {
    console.error(`Runes ${JSON.stringify(usedRunes)} can't pay for spell ${spellID}`);
    return INVALID_MOVE;
  }
  if (!checkSpellParams(spell, params, G)) {
    console.error('Invalid spell params');
    return INVALID_MOVE;
  }

  //////////// Do cast the spell ///////////////

  discardRuneRange(playerID, range, G, ctx);

  addLog(ctx, { type: 'cast', playerID, spellID });
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  spell.effect({ G, ctx, usedRunes, params, spellID });

  player.castedSpells.push(spell.id);
  endTurn(G, ctx);
}

export function passTurn(G: MyGameState<'mut'>, ctx: MyCtx<'mut'>, noMoreCasts = false): MoveReturn {
  getCurrentPlayerState<'mut'>(G).passed = true;

  addLog(ctx, { type: 'pass-turn', playerID: G.turn.currentPlayerID, noMoreCasts });
  endTurn(G, ctx);
}

export function shuffleHand(G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): MoveReturn {
  const player = getCurrentPlayerState<'mut'>(G);
  if (player.shuffleUses <= 0) {
    console.error(`Player ${G.turn.currentPlayerID} tries to shuffle their hand without remaining uses.`);
    return INVALID_MOVE;
  }

  addLog(ctx, { type: 'use-shuffle', playerID: G.turn.currentPlayerID });
  playerShuffleHand(G.turn.currentPlayerID, G, ctx);
  player.shuffleUses -= 1;
}

export const DISCARD_MOVE_HP_COST = 2;
export function discardMove(G: MyGameState<'mut'>, ctx: MyCtx<'mut'>, runeID: RuneID): MoveReturn {
  const playerID = G.turn.currentPlayerID;

  if (!canUseDiscardMove(playerID, G)) {
    console.error(`Player ${playerID} can't discard`);
    return INVALID_MOVE;
  }
  if (!isRuneInHand(playerID, runeID, G)) {
    console.error(`Player ${playerID} tries to discard ${runeID}.`);
    return INVALID_MOVE;
  }

  discardRunes(playerID, [runeID], G, ctx);
  loseHealth(playerID, DISCARD_MOVE_HP_COST, G, ctx);

  if (isDead(playerID, G)) {
    endTurn(G, ctx);
  }
}

export function submitGiftsChoice(G: MyGameState<'mut'>, ctx: MyCtx<'mut'>, runeTypes: RuneType[]): MoveReturn {
  if (G.turn.phase !== 'runeGifts') {
    console.error('Trying to get rune gifts in the wrong phase');
    return INVALID_MOVE;
  }
  if (!checkOneSpellParam(G.turn.paramDef, runeTypes, G)) {
    console.error(`Invalid gifts choice: ${JSON.stringify(runeTypes)}`);
  }

  gainRunes(
    G.turn.currentPlayerID,
    runeTypes.map((type) => createRune(type, G)),
    G,
    ctx
  );
  G.turn.giftedPlayers.push(G.turn.currentPlayerID);
  endTurn(G, ctx);
}
