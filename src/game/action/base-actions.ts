import { PlayerID } from 'boardgame.io';
import { deepCopy, deepEqual } from '../utils';
import { MyCtx } from '../Game';
import { Rune, RuneID } from '../Rune';
import { Enchantment, EnchantmentID, TickType, trigger } from '../spell/Enchantment';
import { HandRange, findRuneIndex, getPlayerState, rangeToRuneIDs } from '../state/getters';
import { STARTING_HEALTH } from '../state/init';
import { Log } from '../state/Log';
import { MyGameState } from '../state/State';

/**
 * Append a line to the logs.
 */
export function addLog(ctx: MyCtx<'mut'>, log: Log): void {
  ctx.effects!.newLog({ log: deepCopy(log) });
}

/**
 * Clear a player reserve and return the runes it contained in an array.
 */
function getAndClearReserve(playerID: PlayerID, G: MyGameState<'mut'>): Rune[] {
  const player = getPlayerState<'mut'>(playerID, G);
  const runes = player.reserve;
  player.reserve = [];

  return runes;
}

/**
 * Add one or more runes to a player reserve.
 */
function _addToReserve(playerID: PlayerID, runes: readonly Rune[], G: MyGameState<'mut'>): void {
  const player = getPlayerState<'mut'>(playerID, G);
  player.reserve = player.reserve.concat(runes);
}

/**
 * The given player obtains the given runes in their reserve.
 */
export function gainRunes(playerID: PlayerID, runes: readonly Rune[], G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  _addToReserve(playerID, runes, G);
  for (const rune of runes) {
    ctx.effects!.gainedRune({ playerID, rune: deepCopy<Rune>(rune) });
  }
  addLog(ctx, { type: 'gain-runes', playerID, runes });
}

/**
 * Discard runes from indexes.
 */
export function discardRunes(playerID: PlayerID, runes: RuneID[], G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  const hand = getPlayerState<'mut'>(playerID, G).hand;
  const discardedRunes: Rune[] = [];

  for (const runeID of runes) {
    const runeIndex = hand.findIndex((r) => r.id === runeID);
    if (runeIndex < 0) {
      throw new Error(`Player ${playerID} can't discard rune ${runeID}`);
    }
    const rune = hand.splice(runeIndex, 1)[0];
    discardedRunes.push(rune);
    rune.timesDiscarded += 1;
    ctx.effects!.discardedRune({ playerID, rune: deepCopy(rune) }, `^0->0`);
  }
  _addToReserve(playerID, discardedRunes, G);
  addLog(ctx, { type: 'discard', playerID, runes: discardedRunes });
}

/**
 * Discard consecutive runes from hand into the reserve
 */
export function discardRuneRange(playerID: PlayerID, range: HandRange, G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  discardRunes(playerID, rangeToRuneIDs(playerID, range, G), G, ctx);
}

/**
 * Discard all runes from a player hand into their reserve.
 */
export function playerDiscardAll(playerID: PlayerID, G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  const { hand } = getPlayerState(playerID, G);
  if (hand.length >= 1) {
    discardRuneRange(playerID, { from: hand[0].id, to: hand[hand.length - 1].id }, G, ctx);
  }
}

/**
 * Put all runes a user controls from their reserve to their hand.
 */
export function playerDrawAll(playerID: PlayerID, G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  const player = getPlayerState<'mut'>(playerID, G);

  const runes = getAndClearReserve(playerID, G);
  player.hand = player.hand.concat(ctx.random!.Shuffle(runes));

  for (const rune of player.hand) {
    ctx.effects!.drewRune({ playerID, rune: deepCopy(rune), all: true });
  }

  addLog(ctx, { type: 'draw', playerID, runes: player.hand });
}

/**
 * Shuffle the given player hand.
 */
export function playerShuffleHand(playerID: PlayerID, G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  const player = getPlayerState<'mut'>(playerID, G);
  player.hand = ctx.random!.Shuffle(player.hand);

  addLog(ctx, { type: 'shuffle', playerID });
}

/**
 * Inflict damage to the given player. The health can go below 0.
 */
export function damage(playerID: PlayerID, amount: number, G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  const { damage: modifiedAmount } = trigger('onBeforeSufferDamage', { G, ctx, playerID, params: { damage: amount } });

  addLog(ctx, { type: 'suffer-damage', playerID, amount: modifiedAmount });
  loseHealth(playerID, modifiedAmount, G, ctx);
}

/**
 * Inflict pure health lost, is not modified by boosts or protection.
 */
export function loseHealth(playerID: PlayerID, amount: number, G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  const player = getPlayerState<'mut'>(playerID, G);

  const realAmount = Math.max(0, amount);
  player.health -= realAmount;
  ctx.effects!.lostHealth({ playerID, amount: realAmount });

  addLog(ctx, { type: 'lose-health', playerID, amount: realAmount });
}

/**
 * Heal the given player. Health can't go beyong STARTING_HEALTH.
 */
export function heal(playerID: PlayerID, amount: number, G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  const player = getPlayerState<'mut'>(playerID, G);

  const realAmount = Math.min(amount, STARTING_HEALTH - player.health);
  ctx.effects!.gainedHealth({ playerID, amount: realAmount });
  player.health = player.health + realAmount;

  addLog(ctx, { type: 'gain-health', playerID, amount: realAmount });
}

/**
 * Trash runes from the reserve.
 */
export function destroyRunesFromReserve(
  playerID: PlayerID,
  runeIDs: readonly RuneID[],
  G: MyGameState<'mut'>,
  ctx: MyCtx<'mut'>
): void {
  const reserve = getPlayerState<'mut'>(playerID, G).reserve;

  const destroyedRunes: Rune[] = [];
  for (const runeID of runeIDs) {
    const index = findRuneIndex(reserve, runeID);

    if (index === null) {
      throw new Error(`Player ${playerID} can't destroy rune ${runeID} from reserve`);
    }
    const rune = reserve.splice(index, 1)[0];
    destroyedRunes.push(rune);
    ctx.effects!.destroyedRuneFromReserve({ playerID, rune: deepCopy(rune) });
  }
  addLog(ctx, { type: 'destroy-runes-from-reserve', playerID, runes: destroyedRunes });
}

/**
 * Destroy runes from hand.
 */
export function destroyRunesFromHand(
  playerID: PlayerID,
  runeIDs: RuneID[],
  G: MyGameState<'mut'>,
  ctx: MyCtx<'mut'>
): void {
  const hand = getPlayerState<'mut'>(playerID, G).hand;

  const destroyedRunes: Rune[] = [];
  for (const runeID of runeIDs) {
    const index = findRuneIndex(hand, runeID);

    if (index === null) {
      throw new Error(`Player ${playerID} can't destroy rune ${runeID} from hand`);
    }
    const rune = hand.splice(index, 1)[0];
    destroyedRunes.push(rune);
    ctx.effects!.destroyedRuneFromHand({ playerID, rune: deepCopy(rune) });
  }
  addLog(ctx, { type: 'destroy-runes-from-hand', playerID, runes: destroyedRunes });
}

/**
 * Give shuffle uses to the given player.
 */
export function gainShuffleUses(playerID: PlayerID, amount: number, G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  addLog(ctx, { type: 'gain-shuffle-uses', amount, playerID });

  getPlayerState<'mut'>(playerID, G).shuffleUses += amount;
}

/**
 * Increase the given player max casts.
 */
export function gainCasts(playerID: PlayerID, amount: number, G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  addLog(ctx, { type: 'gain-casts', amount, playerID });

  getPlayerState<'mut'>(playerID, G).maxCasts += amount;
}

/**
 * Add an enchantment to given player.
 */
export function addEnchantment(
  playerID: PlayerID,
  enchantment: Enchantment<'mut'>,
  G: MyGameState<'mut'>,
  ctx: MyCtx<'mut'>
): void {
  const playerEnchantments = getPlayerState<'mut'>(playerID, G).enchantments;

  const stackableEnchantment =
    enchantment.stackable &&
    playerEnchantments.find((e) => e.name === enchantment.name && deepEqual(e.duration, enchantment.duration));
  if (stackableEnchantment) {
    stackableEnchantment.stacks += enchantment.stacks;
  } else {
    playerEnchantments.push(enchantment);
  }
  addLog(ctx, { type: 'add-enchantment', playerID, enchantment });
}

/**
 * Remove an enchantment of the given player.
 */
function removeEnchantment(playerID: PlayerID, enchantmentID: EnchantmentID, G: MyGameState<'mut'>): void {
  const player = getPlayerState<'mut'>(playerID, G);

  const index = player.enchantments.findIndex((e) => e.id === enchantmentID);
  if (index < 0) {
    throw new Error(`Can't remove enchantment ${enchantmentID} from player ${playerID}: not found.`);
  }

  player.enchantments.splice(index, 1);
}

/**
 * Apply a tick of the given type to the given enchantment (which should belong to the given player)
 */
export function enchantmentTick(
  playerID: PlayerID,
  enchantment: Enchantment<'mut'>,
  tickType: TickType,
  G: MyGameState<'mut'>,
  ctx: MyCtx<'mut'>
): void {
  if (
    enchantment.duration === 'infinite' ||
    enchantment.duration[tickType] === undefined ||
    (tickType === 'turns' && enchantment.turnCreated !== G.turn.currentPlayerID)
  ) {
    return;
  }
  enchantment.duration[tickType]! -= 1;
  if (enchantment.duration[tickType]! <= 0) {
    removeEnchantment(playerID, enchantment.id, G);
    addLog(ctx, { type: 'enchantment-expire', playerID, enchantment });
  }
}

/**
 * Apply a tick of the given type to every enchantment of the given player.
 */
export function allEnchantmentsTick(
  playerID: PlayerID,
  tickType: TickType,
  G: MyGameState<'mut'>,
  ctx: MyCtx<'mut'>
): void {
  const player = getPlayerState<'mut'>(playerID, G);

  for (const enchantment of player.enchantments) {
    enchantmentTick(playerID, enchantment, tickType, G, ctx);
  }
}
