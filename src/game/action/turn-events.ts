import { PlayerID } from 'boardgame.io';
import { checkEndState } from '../EndState';
import { MyCtx } from '../Game';
import { trigger } from '../spell/Enchantment';
import { basicRunesParam } from '../spell/ParamDef';
import { getAllPlayers, getCurrentPlayerState, getLivingPlayerIDs, getPlayerState, isDead } from '../state/getters';
import { DEFAULT_CASTS_PER_TURN, GIFTS_COUNT } from '../state/init';
import { MyGameState } from '../state/State';
import { addLog, allEnchantmentsTick, playerDiscardAll, playerDrawAll } from './base-actions';
import { passTurn } from './moves';

/**
 * End round of the normal phase:
 * - discard runes
 * - tick enchantments
 */
export function endNormalRound(G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  const playerIDs = getLivingPlayerIDs(G);

  for (const playerID of playerIDs) {
    playerDiscardAll(playerID, G, ctx);
  }

  for (const playerID of playerIDs) {
    allEnchantmentsTick(playerID, 'rounds', G, ctx);
  }

  beginRound(G, ctx);
}
/**
 * Start a new round:
 * - Every player discards and redraws all their runes.
 * - All temporary effects are cleared.
 * - Round tick on all players' enchantments.
 */
function beginRound(G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  addLog(ctx, { type: 'new-round' });
  const playerIDs = getLivingPlayerIDs(G);
  for (const playerID of playerIDs) {
    const player = getPlayerState<'mut'>(playerID, G);
    player.passed = false;
    player.maxCasts = DEFAULT_CASTS_PER_TURN;
    player.castedSpells = [];
  }
  G.roundIndex += 1;

  for (const playerID of playerIDs) {
    trigger('onBeginRound', { G, ctx, playerID, params: {} });
  }

  moveToGiftsPhase(G, ctx);
}

function moveToNormalPhase(G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  G.turn = {
    phase: 'normal',
    currentPlayerID: G.turn.currentPlayerID,
  };
  for (const playerID of getLivingPlayerIDs(G)) {
    playerDrawAll(playerID, G, ctx);
  }
  beginNormalTurn(G, ctx);
}

function moveToGiftsPhase(G: MyGameState<'mut'>, ctx: MyCtx): void {
  const giftsCount = GIFTS_COUNT[Math.min(G.roundIndex, GIFTS_COUNT.length - 1)];
  if (giftsCount > 0) {
    G.turn = {
      phase: 'runeGifts',
      currentPlayerID: G.turn.currentPlayerID,
      giftedPlayers: [],
      paramDef: basicRunesParam(giftsCount),
    };
  } else {
    moveToNormalPhase(G, ctx);
  }
}

/**
 * End of turn (any phase):
 * - check if game ended
 * - find the next player
 * - check for phase change
 * - begin the next turn
 */
export function endTurn(G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  G.gameover = checkEndState(G);
  if (G.gameover) {
    return;
  }

  const currentIndex = ctx.playOrder.findIndex((id) => id === G.turn.currentPlayerID)!;

  let nextPlayerID: PlayerID | null = null;
  for (let offset = 1; offset < ctx.numPlayers; offset++) {
    const playerID = ctx.playOrder[(currentIndex + offset) % ctx.playOrder.length];
    if (!isDead(playerID, G)) {
      nextPlayerID = playerID;
      break;
    }
  }

  if (nextPlayerID === null) {
    throw new Error("Invalid state: could't find the next player");
  }
  ctx.events?.endTurn?.({ next: nextPlayerID });
  G.turn.currentPlayerID = nextPlayerID;
  if (G.turn.phase === 'runeGifts' && getLivingPlayerIDs(G).length === G.turn.giftedPlayers.length) {
    moveToNormalPhase(G, ctx);
  } else if (G.turn.phase === 'normal') {
    if (getAllPlayers(G).every(({ playerID, playerState }) => playerState.passed || isDead(playerID, G))) {
      endNormalRound(G, ctx);
    } else {
      beginNormalTurn(G, ctx);
    }
  }
}

/**
 * Turn begining:
 * - Turn tick on the current player's enchantments.
 * - reset pass.
 */
export function beginNormalTurn(G: MyGameState<'mut'>, ctx: MyCtx<'mut'>): void {
  addLog(ctx, { type: 'begin-turn', playerID: G.turn.currentPlayerID });
  const player = getCurrentPlayerState<'mut'>(G);
  player.passed = false;

  for (const playerID of getLivingPlayerIDs(G)) {
    allEnchantmentsTick(playerID, 'turns', G, ctx);
  }

  if (isDead(G.turn.currentPlayerID, G)) {
    endTurn(G, ctx);
  } else if (player.castedSpells.length >= player.maxCasts) {
    passTurn(G, ctx, true);
  }
}
