import { PlayerID } from 'boardgame.io';
import { getLivingPlayerIDs } from './state/getters';
import { MyGameState } from './state/State';

export type EndState = { readonly type: 'win'; readonly winner: PlayerID } | { readonly type: 'draw' };

export function checkEndState(G: MyGameState): undefined | EndState {
  const alivePlayers = getLivingPlayerIDs(G);

  if (alivePlayers.length === 0) {
    return { type: 'draw' };
  } else if (alivePlayers.length === 1) {
    return { type: 'win', winner: alivePlayers[0] };
  }
}
