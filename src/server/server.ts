import { MyGame } from '../game/Game';
import { Server } from 'boardgame.io/server';
import { promises as fs } from 'fs';
import cors from '@koa/cors';
import { env } from 'process';

const BUILD_FOLDER = `${__dirname}/../../build`;

const PORT = Number.parseInt(env.PORT ?? '8000');

async function main(): Promise<void> {
  try {
    const server = Server({ games: [MyGame] });
    server.app.use(cors({ origin: 'http://localhost:3000' })); // for development
    server.app.use(async (ctx, next) => {
      try {
        const path = BUILD_FOLDER + (ctx.req.url === '/' ? '/index.html' : ctx.req.url ?? '');
        const data = await fs.readFile(path);
        ctx.res.writeHead(200);
        ctx.res.end(data);
      } catch (err) {
        await next();
      }
    });
    await server.run(PORT);
  } catch (e) {
    console.error('Uncatched error', e);
  }
}

void main();
