import React from 'react';
import { Lobby } from 'boardgame.io/react';
import { MyGame } from './game/Game';
import { MyGameBoard } from './view/Board';

const PORT = window.location.hostname === 'localhost' ? 8000 : window.location.port;
const URL = `${window.location.protocol}//${window.location.hostname}:${PORT}`;

export const MyLobby: React.FC = () => (
  <Lobby gameServer={URL} lobbyServer={URL} gameComponents={[{ game: MyGame, board: MyGameBoard }]} />
);
