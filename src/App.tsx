import React, { FC } from 'react';
import { MyLobby } from './Lobby';

const App: FC = () => (
  <div>
    {/* <MyGameClient /> */}
    <MyLobby />
  </div>
);

export default App;
