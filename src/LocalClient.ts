import { Client } from 'boardgame.io/react';
import { MyGame } from './game/Game';
import { MyGameBoard } from './view/Board';

export const MyGameClient = Client({
  game: MyGame,
  board: MyGameBoard,
  debug: false,
  // multiplayer: Local({ persist: true }),
  numPlayers: 2,
});
