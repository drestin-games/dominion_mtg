import clsx from 'clsx';
import React, { FC, ReactNode, useState } from 'react';
import { useMyEffectListener } from '../game/effects';
import { I18N } from './i18n/i18n';
import { markdownIfString } from './richText';
import { useGameContext } from './state/GameContext';

export const LogsPanel: FC = () => {
  const [renderedLogs, setRenderedLogs] = useState<ReactNode[]>([]);

  const [open, setOpen] = useState(false);
  const { ctx } = useGameContext();

  function toggleOpen(): void {
    setOpen(!open);
  }

  useMyEffectListener(
    'newLog',
    ({ log }) => {
      setRenderedLogs((old) => {
        const newLog = (
          <div key={old.length} className="log">
            {markdownIfString(I18N.log(log, ctx))}
          </div>
        );
        return [...old, newLog];
      });
    },
    []
  );

  const panelClasses = clsx('logs-panel', { open });
  return (
    <div className={panelClasses} onClick={() => toggleOpen()}>
      <div className={'logs-container'}>{renderedLogs}</div>
    </div>
  );
};
