import Markdown, { MarkdownToJSX } from 'markdown-to-jsx';
import React, { Fragment, ReactNode } from 'react';
import { ALL_RUNE_TYPES, RuneType } from '../game/Rune';
import { ANY_RUNE } from '../game/spell/Cost';
import { RuneIcon } from './RuneIcon';

function overrideMarkdownText(text: string): string {
  return text.replaceAll('\n', '\n\n').replaceAll('*', '**');
}

const markdownOptions: MarkdownToJSX.Options = {
  overrides: {
    p: Fragment,
  },
};

function markdown(input: string, key?: number): ReactNode {
  return (
    <Markdown key={key} options={markdownOptions}>
      {input}
    </Markdown>
  );
}

export function richTextWithRunes(...textOrRunes: (string | RuneType)[]): ReactNode {
  return (
    <>
      {textOrRunes.map((elem, i) => {
        if (ALL_RUNE_TYPES.includes(elem as RuneType) || elem === ANY_RUNE) {
          return <RuneIcon key={i} runeType={elem as RuneType | typeof ANY_RUNE} />;
        } else {
          return markdown(overrideMarkdownText(elem), i);
        }
      })}
    </>
  );
}

export function markdownIfString(input: string | ReactNode): ReactNode {
  if (typeof input === 'string') {
    return markdown(overrideMarkdownText(input));
  } else {
    return input;
  }
}
