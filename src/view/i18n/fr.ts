import { PlayerID } from 'boardgame.io';
import { ReactNode } from 'react';
import { EndState } from '../../game/EndState';
import { CastConditions } from '../../game/spell/CastConditions';
import { Enchantment, EnchantmentName, TickType } from '../../game/spell/Enchantment';
import { BasicRunesParamType, ParamDef } from '../../game/spell/ParamDef';
import { SpellID, getSpell } from '../../game/spell/Spell';
import { Log } from '../../game/state/Log';
import { markdownIfString, richTextWithRunes } from '../richText';
import { ClientContext } from '../state/GameContext';

function pluralize(word: string, number: number): string {
  return number >= 2 ? word + 's' : word;
}

function tradParamType(paramDef: ParamDef): string {
  switch (paramDef.type) {
    case 'player':
      return paramDef.oponentOnly ? 'Adversaire' : 'Joueur';
    case 'basic-runes':
      return `${paramDef.count} ${pluralize('rune', paramDef.count)} ${
        paramDef.distinctOnly ? pluralize('différente', paramDef.count) : ''
      }`;
    case 'hand-runes':
      return `${pluralize('Rune', paramDef.count)} dans la main ${
        paramDef.who === 'self' ? 'du lanceur' : paramDef.who === 'oponent' ? "d'un adversaire" : "d'un joueur"
      }.`;
    case 'reserve-runes':
      return `${pluralize('Rune', paramDef.count)} dans la réserve ${
        paramDef.who === 'self' ? 'du lanceur' : paramDef.who === 'oponent' ? "d'un adversaire" : "d'un joueur"
      }.`;
  }
}

function playerName(playerID: PlayerID, ctx: ClientContext): string {
  return ctx.matchData?.find(({ id }) => id === Number.parseInt(playerID))?.name ?? `Joueur ${playerID}`;
}

function richPlayerName(playerID: PlayerID, ctx: ClientContext): string {
  return `*${playerName(playerID, ctx)}*`;
}

function spell(spellID: SpellID): { title: string; description: ReactNode } {
  const spell = getSpell(spellID);
  return {
    title: spell.frTitle,
    description: markdownIfString(
      richTextWithRunes(...(castConditions(spell.castConditions) + spell.frDescription).split('##'))
    ),
  };
}

function enchantmentName(name: EnchantmentName): string {
  switch (name) {
    case 'vulnerable':
      return 'Vulnérable';
    case 'decay':
      return 'Décomposition';
  }
}

function enchantmentDisplay(enchantment: Enchantment, showDuration = false): string {
  const withoutDuration = enchantmentName(enchantment.name) + (enchantment.stacks > 1 ? ` ×${enchantment.stacks}` : '');
  if (!showDuration || enchantment.duration === 'infinite') {
    return withoutDuration;
  }

  const durations: string[] = [];
  for (const [key, value] of Object.entries(enchantment.duration) as [TickType, number | undefined][]) {
    if (!value) {
      continue;
    }
    const unit = key === 'rounds' ? 'round' : key === 'turns' ? 'tour' : 'activation';
    durations.push(`${value} ${unit}${value >= 2 ? 's' : ''}`);
  }
  return `${withoutDuration} (${durations.join(' ou ')})`;
}

function castConditions({ maxCountPerRound, minIndexInRound, maxIndexInRound }: CastConditions): string {
  if (maxCountPerRound === undefined && minIndexInRound === undefined && maxIndexInRound === undefined) {
    return '';
  }
  const strList: string[] = [];
  if (maxCountPerRound !== undefined) {
    strList.push(`lançable ${maxCountPerRound} fois par round`);
  }
  if (minIndexInRound !== undefined) {
    if (maxIndexInRound !== undefined) {
      strList.push(`${minIndexInRound} <= sorts lancés <= ${maxIndexInRound}`);
    } else {
      strList.push(`sorts lancés >= ${minIndexInRound}`);
    }
  } else if (maxIndexInRound !== undefined) {
    strList.push(`sorts lancés <= ${maxIndexInRound}`);
  }

  return `*Condition: ${strList.join(' et ')}*\n`;
}

export const FR_STRINGS = {
  locale: 'fr',
  board: {
    castButton: 'Lancer le sort',
    resetButton: 'Annuler',
    passButton: 'Passer',
    shuffleButton(remainingUses: number): string {
      return `Mélanger (${remainingUses})`;
    },
    discardButton(hpCost: number): string {
      return `Défausser (-${hpCost}PV)`;
    },
    emptyReserve: 'Réserve vide',
    playerBoardTitle(playerID: PlayerID, ctx: ClientContext): string {
      return playerName(playerID, ctx);
    },
    reserveTooltip: 'Réserve',
    hpTooltip: 'Points de vie',
    enchantmentsTooltip: 'Enchantements',
    castsTooltip: 'Nombre de sorts lancés ce round / limite',
  },
  instructions: {
    selectSpell: 'Sélectionnez un sort à lancer.',
    selectCastingRunes: 'Payez le coût de lancement.',
    selectParam(paramDef: ParamDef): string {
      return `Paramètre de sort: ${tradParamType(paramDef)}`;
    },
    selectDiscardedRune: 'Sélectionnez la rune à défausser.',
    selectGifts: 'Choisissez votre lot de début de round.',
    waitingForPlayer: (name: string, ctx: ClientContext): string => `Tour du joueur ${playerName(name, ctx)}`,
  },
  modal: {
    validate: 'Valider',
    cancel: 'Annuler',
    enchantmentListTitle: (playerID: PlayerID, ctx: ClientContext): string =>
      `Enchantements de ${playerName(playerID, ctx)}`,
    reserveDetailsTitle: (playerID: PlayerID, ctx: ClientContext): string => `Réserve de ${playerName(playerID, ctx)}`,
    gameover(endState: EndState, ctx: ClientContext): string {
      if (endState.type === 'draw') {
        return 'Égalité';
      } else {
        return `Victoire de ${playerName(endState.winner, ctx)}`;
      }
    },
    selectRuneTypesTitle: (paramDef: BasicRunesParamType): string => `Choisissez ${tradParamType(paramDef)}`,
  },
  spell,
  castConditions,
  enchantmentDisplay,
  log: (log: Log, ctx: ClientContext): ReactNode => {
    switch (log.type) {
      case 'use-shuffle':
        return `${richPlayerName(log.playerID, ctx)} utilise un Mélange.`;
      case 'shuffle':
        return `${richPlayerName(log.playerID, ctx)} mélange sa main.`;
      case 'gain-shuffle-uses':
        return `${richPlayerName(log.playerID, ctx)} obtient ${log.amount} ${pluralize('Mélange', log.amount)}.`;
      case 'gain-casts':
        return `${richPlayerName(log.playerID, ctx)} obtient ${log.amount} ${pluralize('Lancer', log.amount)}}.`;
      case 'draw':
        return richTextWithRunes(`${richPlayerName(log.playerID, ctx)} pioche `, ...log.runes.map((r) => r.type), '.');
      case 'discard':
        return richTextWithRunes(
          `${richPlayerName(log.playerID, ctx)} défausse `,
          ...log.runes.map((r) => r.type),
          '.'
        );
      case 'gain-runes':
        return richTextWithRunes(
          `${richPlayerName(log.playerID, ctx)} obtient `,
          ...log.runes.map((r) => r.type),
          ' dans sa réserve.'
        );
      case 'gain-health':
        return `${richPlayerName(log.playerID, ctx)} gagne ${log.amount} PV.`;
      case 'lose-health':
        return `${richPlayerName(log.playerID, ctx)} perd ${log.amount} PV.`;
      case 'suffer-damage':
        return `${richPlayerName(log.playerID, ctx)} subit ${log.amount} ${pluralize('dégat', log.amount)}.`;
      case 'new-round':
        return `################ Nouveau round #################`;
      case 'pass-turn':
        return `${richPlayerName(log.playerID, ctx)} passe son tour${
          log.noMoreCasts ? ' (à court de lancer de sorts)' : ''
        }.`;
      case 'cast':
        return `${richPlayerName(log.playerID, ctx)} lance le sort ${spell(log.spellID).title}.`;
      case 'add-enchantment':
        return `${richPlayerName(log.playerID, ctx)} obtient ${enchantmentDisplay(log.enchantment)}.`;
      case 'enchantment-expire':
        return `${richPlayerName(log.playerID, ctx)} perd ${enchantmentDisplay(log.enchantment)}.`;
      case 'trigger-enchantment':
        return `${richPlayerName(log.playerID, ctx)} – ${enchantmentDisplay(log.enchantment)} prend effet.`;
      case 'begin-turn':
        return `---- Début du tour de ${richPlayerName(log.playerID, ctx)} ----`;
      case 'destroy-runes-from-hand':
        return richTextWithRunes(
          `${richPlayerName(log.playerID, ctx)} détruit `,
          ...log.runes.map((r) => r.type),
          ' dans sa main.'
        );
      case 'destroy-runes-from-reserve':
        return richTextWithRunes(
          `${richPlayerName(log.playerID, ctx)} détruit `,
          ...log.runes.map((r) => r.type),
          ' dans sa réserve.'
        );
    }
  },
};
