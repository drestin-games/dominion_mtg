import { Immutable } from '../../game/utils';
import { FR_STRINGS } from './fr';

type Tranlations = Immutable<typeof FR_STRINGS>;

export const I18N: Tranlations = FR_STRINGS;
