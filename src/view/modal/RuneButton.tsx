import clsx from 'clsx';
import React, { FC } from 'react';
import { RuneType } from '../../game/Rune';
import { RuneIcon } from '../RuneIcon';

interface Props {
  onClick: () => void;
  runeType: RuneType;
  selectedCount: number;
}

export const RuneButton: FC<Props> = ({ onClick, runeType, selectedCount }) => {
  const selected = selectedCount >= 1;
  const classes = clsx('rune-button', { selected });
  return (
    <div className={classes} onClick={() => onClick()}>
      <RuneIcon runeType={runeType} />
      {selected ? (
        <div className="selected-count">
          <div>{selectedCount}</div>
        </div>
      ) : null}
    </div>
  );
};
