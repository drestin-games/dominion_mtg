import React, { FC, useEffect, useState } from 'react';
import { BASIC_RUNE_TYPES, RuneType } from '../../game/Rune';
import { BasicRunesParamType } from '../../game/spell/ParamDef';
import { arrayCount } from '../../game/utils';
import { I18N } from '../i18n/i18n';
import { useGameContext } from '../state/GameContext';
import { isSelectingRuneTypes } from '../state/getters';
import { ViewStep } from '../state/ViewState';
import { Modal } from './Modal';
import { RuneButton } from './RuneButton';

export const SelectRuneTypeModal: FC = () => {
  const { view, selectParam, cancel } = useGameContext();
  const [selectedRuneTypes, setSelectedRuneTypes] = useState<RuneType[]>([]);

  const visible = isSelectingRuneTypes(view);

  useEffect(() => {
    if (!visible && selectedRuneTypes.length > 0) {
      setSelectedRuneTypes([]);
    }
  }, [visible, selectedRuneTypes]);

  if (!visible) {
    return null;
  }

  const canCancel = view.step === ViewStep.selectParams;
  const paramDef = view.currentParamDef as BasicRunesParamType;
  const numberToSelect = paramDef.count;

  function onChoose(runeType: RuneType): void {
    if (numberToSelect === 1) {
      selectParam([runeType]);
    } else if (
      selectedRuneTypes.length < numberToSelect &&
      !(paramDef.distinctOnly && selectedRuneTypes.includes(runeType))
    ) {
      const newSelected = [...selectedRuneTypes, runeType];
      if (numberToSelect === newSelected.length) {
        selectParam(newSelected);
      } else {
        setSelectedRuneTypes(newSelected);
      }
    }
  }

  function onClickCancel(): void {
    if (canCancel) {
      cancel();
    }
  }

  return (
    <Modal minimizable title={I18N.modal.selectRuneTypesTitle(paramDef)}>
      <div className="select-rune-types">
        <div className="runes">
          {BASIC_RUNE_TYPES.map((type) => (
            <RuneButton
              key={type}
              runeType={type}
              onClick={() => onChoose(type)}
              selectedCount={arrayCount(selectedRuneTypes, (e) => e === type)}
            />
          ))}
        </div>
        <div className="buttonsContainer">
          {canCancel ? <button onClick={() => onClickCancel()}>{I18N.modal.cancel}</button> : null}
        </div>
      </div>
    </Modal>
  );
};
