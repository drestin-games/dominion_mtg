import clsx from 'clsx';
import React, { FC, ReactNode, useState } from 'react';

interface Props {
  children: ReactNode;
  onOverlayClick?: () => void;
  minimizable?: boolean;
  title?: string;
}

export const Modal: FC<Props> = ({ onOverlayClick, children, minimizable, title }) => {
  const [minimized, setMinimized] = useState(false);

  function onClick(): void {
    if (onOverlayClick) {
      onOverlayClick();
    }
    if (minimizable) {
      setMinimized(!minimized);
    }
  }

  const classes = clsx('overlay', { minimized });
  return (
    <div className={classes} onClick={onClick}>
      {/* Don't propagate clicks on overlay when clicking the modal */}
      <div
        className="modal"
        onClick={(event) => event.stopPropagation()}
        onClickCapture={(event) => {
          // Stop event when clicking the modal while minimized, and toggle minimization
          if (minimized) {
            event.stopPropagation();
            onClick();
          }
        }}
      >
        {title ? <div className="modal-title">{title}</div> : null}
        <div className="modal-content">{children}</div>
      </div>
    </div>
  );
};
