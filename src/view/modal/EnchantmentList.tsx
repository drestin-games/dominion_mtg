import { PlayerID } from 'boardgame.io';
import React, { FC, useContext } from 'react';
import { getPlayerState } from '../../game/state/getters';
import { ModalsControlContext } from '../Board';
import { I18N } from '../i18n/i18n';
import { useGameContext } from '../state/GameContext';
import { Modal } from './Modal';

interface Props {
  playerID: PlayerID | null;
}

export const EnchantmentList: FC<Props> = ({ playerID }) => {
  if (playerID === null) {
    return null;
  }

  const { setEnchantmentListPlayerID } = useContext(ModalsControlContext)!;

  const { G, ctx } = useGameContext();
  const player = getPlayerState(playerID, G);

  return (
    <Modal
      onOverlayClick={() => setEnchantmentListPlayerID(null)}
      title={I18N.modal.enchantmentListTitle(playerID, ctx)}
    >
      <div className="enchantment-list">
        {player.enchantments.map((enchantment) => (
          <div className="item" key={enchantment.id}>
            {I18N.enchantmentDisplay(enchantment, true)}
          </div>
        ))}
      </div>
    </Modal>
  );
};
