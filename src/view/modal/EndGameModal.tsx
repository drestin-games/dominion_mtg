import React, { FC } from 'react';
import { I18N } from '../i18n/i18n';
import { useGameContext } from '../state/GameContext';
import { Modal } from './Modal';

export const EndGameModal: FC = () => {
  const { G, ctx } = useGameContext();
  if (G.gameover === undefined) {
    return null;
  }

  return (
    <Modal>
      <div className="gameover">{I18N.modal.gameover(G.gameover, ctx)}</div>
    </Modal>
  );
};
