import { PlayerID } from 'boardgame.io';
import React, { FC, useContext } from 'react';
import { getPlayerState } from '../../game/state/getters';
import { ModalsControlContext } from '../Board';
import { I18N } from '../i18n/i18n';
import { ReserveRune } from '../player/ReserveRune';
import { useGameContext } from '../state/GameContext';
import { Modal } from './Modal';

interface Props {
  playerID: PlayerID | null;
}

export const ReserveDetailsModal: FC<Props> = ({ playerID }) => {
  if (playerID === null) {
    return null;
  }

  const { setReserveDetailsPlayerID } = useContext(ModalsControlContext)!;

  const { G, ctx } = useGameContext();
  const player = getPlayerState(playerID, G);

  return (
    <Modal onOverlayClick={() => setReserveDetailsPlayerID(null)} title={I18N.modal.reserveDetailsTitle(playerID, ctx)}>
      <div className="reserve-details">
        {player.reserve.map((rune) => (
          <ReserveRune key={rune.id} rune={rune} playerID={playerID} />
        ))}
      </div>
    </Modal>
  );
};
