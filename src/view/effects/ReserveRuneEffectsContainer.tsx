import { PlayerID } from 'boardgame.io';
import React, { FC, useState } from 'react';
import { useMyEffectListener } from '../../game/effects';
import { RuneType } from '../../game/Rune';
import { Position } from '../../game/utils';
import { ReserveRuneEffect } from './ReserveRuneEffect';

interface Props {
  playerID: PlayerID;
  getReservePosition: () => Position;
}

let nextEffectID = 0;

interface EffectConfig {
  id: number;
  from: Position;
  to: Position;
  rune: RuneType;
}
export const ReserveRuneEffectsContainer: FC<Props> = ({ playerID, getReservePosition }) => {
  const [effectConfigs, setEffectConfigs] = useState<EffectConfig[]>([]);

  function removeEffect(id: number): void {
    const index = effectConfigs.findIndex((v) => v.id === id);
    if (index >= 0) {
      const newEffectIDs = [...effectConfigs.slice(0, index), ...effectConfigs.slice(index + 1)];
      setEffectConfigs(newEffectIDs);
    }
  }

  function newEffect(runeType: RuneType, positions: () => { from: Position; to: Position }): void {
    const newEffect: EffectConfig = {
      id: nextEffectID,
      ...positions(),
      rune: runeType,
    };
    nextEffectID += 1;

    setEffectConfigs([...effectConfigs, newEffect]);
  }

  function useRuneEffectListener(
    effectName: 'gainedRune' | 'destroyedRuneFromReserve',
    positions: () => { from: Position; to: Position }
  ): void {
    useMyEffectListener(
      effectName,
      (event) => {
        if (event.playerID === playerID) {
          newEffect(event.rune.type, positions);
        }
      },
      []
    );
  }

  useRuneEffectListener('gainedRune', () => {
    const reservePos = getReservePosition();
    return {
      from: {
        x: reservePos.x + 200,
        y: reservePos.y - 200,
      },
      to: reservePos,
    };
  });
  useRuneEffectListener('destroyedRuneFromReserve', () => {
    const reservePos = getReservePosition();
    return {
      from: reservePos,
      to: {
        x: reservePos.x,
        y: reservePos.y - 75,
      },
    };
  });

  return (
    <>
      {effectConfigs.map((config) => (
        <ReserveRuneEffect key={config.id} {...config} onFinish={() => removeEffect(config.id)} />
      ))}
    </>
  );
};
