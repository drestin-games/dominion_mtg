import anime from 'animejs';
import React, { FC, useEffect, useRef } from 'react';
import { RuneType } from '../../game/Rune';
import { Position } from '../../game/utils';
import { RuneIcon } from '../RuneIcon';

interface Props {
  from: Position;
  to: Position;
  rune: RuneType;
  onFinish: () => void;
}

export const ReserveRuneEffect: FC<Props> = ({ from, to, rune, onFinish }) => {
  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    anime({
      targets: ref.current,
      translateX: [from.x, to.x],
      translateY: [from.y, to.y],
      opacity: [1, 0],
      complete: () => onFinish(),
      easing: 'easeOutQuad',
      duration: 1000,
    });
  }, []);
  return (
    <div ref={ref} className="reserve-rune-effect">
      <RuneIcon runeType={rune} />
    </div>
  );
};
