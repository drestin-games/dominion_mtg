export function imagePath(fileName: string): string {
  return `/assets/${fileName}`;
}
