import { PlayerID } from 'boardgame.io';
import clsx from 'clsx';
import React, { FC } from 'react';
import { Rune } from '../../game/Rune';
import { RuneIcon } from '../RuneIcon';
import { useGameContext } from '../state/GameContext';
import { isReserveRuneSelectable, isRuneSelected } from '../state/getters';

interface Props {
  rune: Rune;
  playerID: PlayerID;
}

export const ReserveRune: FC<Props> = ({ rune, playerID }) => {
  const { view, G, selectReserveRune } = useGameContext();
  const selectable = isReserveRuneSelectable(playerID, rune.id, view, G);

  function onClick(): void {
    if (selectable) {
      selectReserveRune({ playerID, runeID: rune.id });
    }
  }

  const classes = clsx('rune', { 'proposable-valid': selectable }, isRuneSelected(playerID, rune.id, view, G));
  return (
    <div className={classes} onClick={() => onClick()}>
      <RuneIcon runeType={rune.type} />
    </div>
  );
};
