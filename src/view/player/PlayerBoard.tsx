import { PlayerID } from 'boardgame.io';
import clsx from 'clsx';
import React, { FC, useRef } from 'react';
import { DISCARD_MOVE_HP_COST, checkOneSpellParam } from '../../game/action/moves';
import { canUseDiscardMove, getPlayerState, isDead } from '../../game/state/getters';
import { Position } from '../../game/utils';
import { ReserveRuneEffectsContainer } from '../effects/ReserveRuneEffectsContainer';
import { I18N } from '../i18n/i18n';
import { useGameContext } from '../state/GameContext';
import { isInStep } from '../state/getters';
import { ViewStep } from '../state/ViewState';
import { EnchantmentButton } from './EnchantmentButton';
import { PlayerHand } from './PlayerHand';
import { PlayerHealth } from './PlayerHealth';
import { PlayerRemainingCasts } from './PlayerRemainingCasts';
import { ReserveDisplay } from './ReserveDisplay';

interface PlayerBoardProps {
  playerID: PlayerID;
}

export const PlayerBoard: FC<PlayerBoardProps> = ({ playerID }) => {
  const { G, ctx, view, passTurn, cancel, selectParam, shuffleHand, chooseDiscardMove } = useGameContext();
  const { reserve, shuffleUses } = getPlayerState(playerID, G);
  const ref = useRef<HTMLDivElement>(null);

  const isCurrent = playerID === G.turn.currentPlayerID;
  const canPassTurn = isCurrent && isInStep(ViewStep.selectSpell, view);
  const canCancel =
    isCurrent &&
    (isInStep(ViewStep.selectCastingRunes, view) ||
      isInStep(ViewStep.selectDiscardedRune, view) ||
      isInStep(ViewStep.selectParams, view));
  const canShuffle = isCurrent && isInStep(ViewStep.selectSpell, view) && shuffleUses >= 1;
  const canDiscard = isCurrent && isInStep(ViewStep.selectSpell, view) && canUseDiscardMove(playerID, G);

  const selectable =
    isInStep(ViewStep.selectParams, view) &&
    view.currentParamDef!.type === 'player' &&
    checkOneSpellParam(view.currentParamDef!, playerID, G);
  const selected = Object.values(view.spellParams).includes(playerID);

  function onClick(): void {
    if (selectable) {
      selectParam(playerID);
    }
  }

  function getReservePosition(): Position {
    if (!ref.current) {
      return { x: 0, y: 0 };
    }
    const rect = ref.current.getBoundingClientRect();
    return {
      x: rect.x,
      y: rect.y,
    };
  }

  const classes = clsx('player-board', { current: isCurrent, selectable, selected, dead: isDead(playerID, G) });
  const nameClasses = clsx('name', { me: playerID === ctx.playerID ?? G.turn.currentPlayerID });
  return (
    <div ref={ref} className={classes} onClick={() => onClick()}>
      <div className="header">
        <ReserveDisplay reserve={reserve} playerID={playerID} />
        <div className={nameClasses}>{I18N.board.playerBoardTitle(playerID, ctx)}</div>
        <EnchantmentButton playerID={playerID} />
        <PlayerRemainingCasts playerID={playerID} />
        <PlayerHealth playerID={playerID} />
      </div>
      <PlayerHand playerID={playerID} getReservePosition={getReservePosition} />

      <div className="buttons">
        <button className="shuffle button" disabled={!canShuffle} onClick={() => shuffleHand()}>
          {I18N.board.shuffleButton(shuffleUses)}
        </button>
        <button className="discard button" disabled={!canDiscard} onClick={() => chooseDiscardMove()}>
          {I18N.board.discardButton(DISCARD_MOVE_HP_COST)}
        </button>
        {canCancel ? (
          <button className="reset button" disabled={!canCancel} onClick={() => cancel()}>
            {I18N.board.resetButton}
          </button>
        ) : (
          <button className="pass button" disabled={!canPassTurn} onClick={() => passTurn()}>
            {I18N.board.passButton}
          </button>
        )}
      </div>
      <ReserveRuneEffectsContainer playerID={playerID} getReservePosition={getReservePosition} />
    </div>
  );
};
