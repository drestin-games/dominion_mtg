import anime from 'animejs';
import { PlayerID } from 'boardgame.io';
import clsx from 'clsx';
import React, { FC, useContext, useRef } from 'react';
import { useMyEffectListener } from '../../game/effects';
import { getPlayerState } from '../../game/state/getters';
import { ModalsControlContext } from '../Board';
import { imagePath } from '../constants';
import { I18N } from '../i18n/i18n';
import { useGameContext } from '../state/GameContext';

interface Props {
  playerID: PlayerID;
}

export const EnchantmentButton: FC<Props> = ({ playerID }) => {
  const ref = useRef<HTMLImageElement>(null);
  const { setEnchantmentListPlayerID } = useContext(ModalsControlContext)!;
  const { G } = useGameContext();

  useMyEffectListener(
    'triggeredEnchantment',
    (effect) => {
      if (effect.playerID === playerID) {
        anime({
          targets: ref.current,
          keyframes: [
            {
              scaleX: 1.3,
              scaleY: 1.3,
            },
            {
              scaleX: 1,
              scaleY: 1,
            },
          ],
          duration: 300,
          easing: 'easeInOutQuad',
        });
      }
    },
    []
  );

  const active = getPlayerState(playerID, G).enchantments.length > 0;

  function onClick(): void {
    if (active) {
      setEnchantmentListPlayerID(playerID);
    }
  }

  const src = imagePath(active ? 'enchantments-active.svg' : 'enchantments.svg');

  const classes = clsx('enchantment-button', { active });
  return (
    <img ref={ref} className={classes} onClick={() => onClick()} src={src} title={I18N.board.enchantmentsTooltip}></img>
  );
};
