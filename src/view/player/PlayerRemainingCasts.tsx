import { PlayerID } from 'boardgame.io';
import React, { FC } from 'react';
import { getPlayerState } from '../../game/state/getters';
import { I18N } from '../i18n/i18n';
import { useGameContext } from '../state/GameContext';

interface Props {
  playerID: PlayerID;
}

export const PlayerRemainingCasts: FC<Props> = ({ playerID }) => {
  const { G } = useGameContext();
  const { castedSpells, maxCasts } = getPlayerState(playerID, G);

  return (
    <div className="casts-counter" title={I18N.board.castsTooltip}>
      {castedSpells.length}/{maxCasts}
    </div>
  );
};
