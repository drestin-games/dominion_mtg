import { PlayerID } from 'boardgame.io';
import React, { FC, useContext } from 'react';
import { useMyEffectListener } from '../../game/effects';
import { ALL_RUNE_TYPES } from '../../game/Rune';
import { countRunesOfType, runeCount } from '../../game/state/getters';
import { Reserve } from '../../game/state/State';
import { useDisplayedState } from '../../game/utils';
import { ModalsControlContext } from '../Board';
import { I18N } from '../i18n/i18n';
import { RuneIcon } from '../RuneIcon';

interface ReserveDisplayProps {
  playerID: PlayerID;
  reserve: Reserve;
}

export const ReserveDisplay: FC<ReserveDisplayProps> = ({ playerID, reserve }) => {
  const [displayedReserve, setDisplayedReserve] = useDisplayedState(reserve);
  const { setReserveDetailsPlayerID } = useContext(ModalsControlContext)!;

  function useAddEffectListener(effectName: 'discardedRune' | 'gainedRune'): void {
    useMyEffectListener(
      effectName,
      (e) => {
        if (e.playerID === playerID) {
          setDisplayedReserve((old) => {
            if (old.find((r) => e.rune.id === r.id)) {
              // fix double-discard effect
              return old;
            } else {
              return [...old, e.rune];
            }
          });
        }
      },
      []
    );
  }
  useAddEffectListener('discardedRune');
  useAddEffectListener('gainedRune');

  function useRemoveEffectListener(effectName: 'drewRune' | 'destroyedRuneFromReserve'): void {
    useMyEffectListener(
      effectName,
      (e) => {
        if (e.playerID === playerID) {
          setDisplayedReserve((old) => {
            const index = old.findIndex((r) => r.id === e.rune.id);
            if (index >= 0) {
              return [...old.slice(0, index), ...old.slice(index + 1)];
            } else {
              return old;
            }
          });
        }
      },
      []
    );
  }
  useRemoveEffectListener('drewRune');
  useRemoveEffectListener('destroyedRuneFromReserve');

  const content =
    runeCount(displayedReserve) === 0 ? (
      <p className="empty-reserve">{I18N.board.emptyReserve}</p>
    ) : (
      ALL_RUNE_TYPES.map((type) => {
        const count = countRunesOfType(type, displayedReserve);
        return count >= 1 ? (
          <span key={type} className="item">
            {count}
            <RuneIcon runeType={type} />{' '}
          </span>
        ) : null;
      })
    );
  return (
    <div className="reserve" title={I18N.board.reserveTooltip} onClick={() => setReserveDetailsPlayerID(playerID)}>
      {content}
    </div>
  );
};
