import { PlayerID } from 'boardgame.io';
import React, { FC, useEffect } from 'react';
import { HandRange, getPlayerState } from '../../game/state/getters';
import { Position } from '../../game/utils';
import { useGameContext } from '../state/GameContext';
import { ViewStep } from '../state/ViewState';
import { RuneDisplay } from './HandRune';

interface PlayerHandProps {
  playerID: PlayerID;
  getReservePosition: () => Position;
}

export const PlayerHand: FC<PlayerHandProps> = ({ playerID, getReservePosition }) => {
  const { G, view } = useGameContext();
  const { hand } = getPlayerState(playerID, G);

  const [proposedRange, setProposedRange] = React.useState<HandRange | null>(null);

  useEffect(() => {
    if (view.step !== ViewStep.selectCastingRunes) {
      if (proposedRange !== null) {
        setProposedRange(null);
      }
    }
  }, [view.step]);

  return (
    <div className="hand">
      {hand.map((r, i) => (
        <RuneDisplay
          key={r.id}
          playerID={playerID}
          index={i}
          rune={r}
          proposedRange={proposedRange}
          setProposedRange={setProposedRange}
          getReservePosition={getReservePosition}
        />
      ))}
    </div>
  );
};
