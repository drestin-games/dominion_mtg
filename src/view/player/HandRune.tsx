import anime from 'animejs';
import { PlayerID } from 'boardgame.io';
import clsx from 'clsx';
import React, { FC, useEffect, useRef, useState } from 'react';
import {
  DESTROYED_RUNE_EFFECT_DURATION,
  DISCARD_EFFECT_DURATION,
  DRAW_EFFECT_DURATION,
  useMyEffectListener,
} from '../../game/effects';
import { Rune } from '../../game/Rune';
import { HandRange, canPaySpell, getPlayerState, isInRange, rangeFromOne } from '../../game/state/getters';
import { Position } from '../../game/utils';
import { RuneIcon } from '../RuneIcon';
import { useGameContext } from '../state/GameContext';
import { isHandRuneSelectable, isInStep, isRuneSelected, selectedSpellOneRuneCostOnly } from '../state/getters';
import { ViewStep } from '../state/ViewState';

interface Props {
  playerID: PlayerID;
  rune: Rune;
  index: number;
  proposedRange: HandRange | null;
  setProposedRange: (range: HandRange | null) => void;
  getReservePosition: () => Position;
}

export type Highlight = 'first-selected' | 'proposed-invalid' | 'proposed-valid' | 'selected' | null;

export const RuneDisplay: FC<Props> = ({
  playerID,
  rune,
  index,
  proposedRange,
  setProposedRange,
  getReservePosition,
}) => {
  const ref = useRef<HTMLDivElement>(null);
  const [lastPositionOpt, setLastPosition] = useState<Position | null>(null);
  const { G, view, selectHandRune } = useGameContext();

  function getCurrentPosition(): Position {
    if (ref.current) {
      const rect = ref.current.getBoundingClientRect();
      return { x: rect.x, y: rect.y };
    } else {
      return { x: 0, y: 0 };
    }
  }

  useEffect(() => {
    if (ref.current) {
      const newPosition = getCurrentPosition();

      const lastPosition = lastPositionOpt ?? getReservePosition();

      anime({
        targets: ref.current,
        translateX: [lastPosition.x - newPosition.x, 0],
        translateY: [lastPosition.y - newPosition.y, 0],
        opacity: 1,
        duration: DRAW_EFFECT_DURATION,
        easing: 'easeInOutQuad',
        delay: lastPositionOpt ? 0 : index * 50,
      });
      setLastPosition(newPosition);
    }
  }, [index, getPlayerState(playerID, G).hand.length, rune.timesDiscarded]);

  function animateDisappear(
    effectName: 'discardedRune' | 'destroyedRuneFromHand',
    getTargetPos: () => Position,
    duration: number
  ): void {
    useMyEffectListener(
      effectName,
      (effect) => {
        if (effect.rune.id === rune.id) {
          const currentPos = getCurrentPosition();
          const targetPos = getTargetPos();
          anime({
            targets: ref.current,
            translateX: targetPos.x - currentPos.x,
            translateY: targetPos.y - currentPos.y,
            opacity: 0,
            duration,
            easing: 'easeInOutQuad',
            complete: () => {
              setLastPosition(null);
              anime.set(ref.current, { translateX: 0, translateY: 0, opacity: 0 });
            },
          });
        }
      },
      [lastPositionOpt?.x, lastPositionOpt?.y]
    );
  }

  animateDisappear('discardedRune', () => getReservePosition(), DISCARD_EFFECT_DURATION);
  animateDisappear(
    'destroyedRuneFromHand',
    () => {
      const currentPos = getCurrentPosition();
      return {
        x: currentPos.x,
        y: currentPos.y - 75,
      };
    },
    DESTROYED_RUNE_EFFECT_DURATION
  );

  const isCurrent = playerID === G.turn.currentPlayerID;
  const selectable = isHandRuneSelectable(playerID, rune.id, view, G);

  function onClick(): void {
    if (selectable) {
      selectHandRune({ playerID, runeID: rune.id });
    }
  }

  function onMouseEnter(): void {
    if (isInStep(ViewStep.selectCastingRunes, view) && isCurrent) {
      const range: HandRange | null = selectedSpellOneRuneCostOnly(view)
        ? rangeFromOne(rune.id)
        : view.firstRuneOfRange !== undefined
        ? { from: view.firstRuneOfRange, to: rune.id }
        : null;

      if (range && (proposedRange === null || proposedRange.from !== range.from || proposedRange.to !== range.to)) {
        setProposedRange(range);
      }
    }
  }

  function onMouseLeave(): void {
    if (isInStep(ViewStep.selectCastingRunes, view) && proposedRange !== null) {
      setProposedRange(null);
    }
  }

  function highlight(): string | null {
    if (isInStep(ViewStep.selectCastingRunes, view) && isCurrent) {
      if (proposedRange) {
        if (isInRange(playerID, proposedRange, rune.id, G)) {
          if (canPaySpell(playerID, view.selectedSpell!, proposedRange, G)) {
            return 'proposed-valid';
          } else {
            return 'proposed-invalid';
          }
        }
      } else {
        if (view.firstRuneOfRange === rune.id) {
          return 'first-selected';
        } else if (view.firstRuneOfRange === undefined) {
          return 'first-selectable';
        }
      }
    } else if (selectable) {
      return 'proposable-valid';
    }

    return null;
  }
  const classes = clsx('rune', highlight(), isRuneSelected(playerID, rune.id, view, G));
  return (
    <div
      ref={ref}
      className={classes}
      onClick={() => onClick()}
      onMouseEnter={() => onMouseEnter()}
      onMouseLeave={() => onMouseLeave()}
    >
      <RuneIcon runeType={rune.type} />
    </div>
  );
};
