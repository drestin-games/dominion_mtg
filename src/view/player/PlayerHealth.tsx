import anime from 'animejs';
import { PlayerID } from 'boardgame.io';
import React, { FC, useRef } from 'react';
import { useMyEffectListener } from '../../game/effects';
import { getPlayerState } from '../../game/state/getters';
import { useDisplayedState } from '../../game/utils';
import { I18N } from '../i18n/i18n';
import { useGameContext } from '../state/GameContext';

interface PlayerHealthProps {
  playerID: PlayerID;
}

export const PlayerHealth: FC<PlayerHealthProps> = ({ playerID }) => {
  const { G } = useGameContext();
  const { health } = getPlayerState(playerID, G);
  const [displayedHealth, setDisplayedHealth] = useDisplayedState(health);

  const ref = useRef<HTMLDivElement>(null);

  function animateColor(color: string): void {
    anime({
      targets: ref.current,
      color: [color, 'rgb(0,0,0)'],
      easing: 'easeInQuint',
      duration: 500,
    });
  }

  useMyEffectListener(
    'lostHealth',
    (event) => {
      if (playerID === event.playerID) {
        animateColor('rgb(255,0,0)');
        setDisplayedHealth((h) => h - event.amount);
      }
    },
    []
  );

  useMyEffectListener(
    'gainedHealth',
    (event) => {
      if (playerID === event.playerID) {
        animateColor('rgb(0,192,0)');
        setDisplayedHealth((h) => h + event.amount);
      }
    },
    []
  );

  return (
    <div className="health" ref={ref} title={I18N.board.hpTooltip}>
      {displayedHealth}
    </div>
  );
};
