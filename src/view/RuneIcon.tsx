import React, { FC } from 'react';
import { RuneType } from '../game/Rune';
import { ANY_RUNE } from '../game/spell/Cost';
import { imagePath } from './constants';

interface RuneIconProps {
  runeType: RuneType | typeof ANY_RUNE;
}

export const RuneIcon: FC<RuneIconProps> = ({ runeType: rune }) => {
  const src = imagePath(`rune-${rune}.svg`);
  return <img draggable={false} className="rune-icon" src={src} alt={rune}></img>;
};
