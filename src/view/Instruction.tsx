import React, { FC } from 'react';
import { MyGameState } from '../game/state/State';
import { I18N } from './i18n/i18n';
import { ClientContext, useGameContext } from './state/GameContext';
import { ViewState, ViewStep } from './state/ViewState';

function instruction({ step, currentParamDef, waiting }: ViewState, G: MyGameState, ctx: ClientContext): string {
  if (waiting && ctx.playerID !== G.turn.currentPlayerID) {
    return I18N.instructions.waitingForPlayer(G.turn.currentPlayerID, ctx);
  }
  if (step === ViewStep.selectParams) {
    return I18N.instructions.selectParam(currentParamDef!);
  } else if (step) {
    return I18N.instructions[step];
  } else {
    return '';
  }
}

export const Instruction: FC = () => {
  const { view, ctx, G } = useGameContext();

  return <div className="instruction">{instruction(view, G, ctx)}</div>;
};
