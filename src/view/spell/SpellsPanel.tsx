import React, { FC } from 'react';
import { SpellCard } from './SpellCard';
import { useGameContext } from '../state/GameContext';

export const SpellsPanel: FC = () => {
  const { G } = useGameContext();
  return (
    <div className="spells-panel">
      {G.availableSpells.map((spellID) => (
        <SpellCard key={spellID} spellID={spellID} />
      ))}
    </div>
  );
};
