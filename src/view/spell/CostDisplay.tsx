import clsx from 'clsx';
import React, { FC, ReactNode } from 'react';
import { ANY_RUNE, Cost, CostRune, SimpleCostRune } from '../../game/spell/Cost';
import { RuneIcon } from '../RuneIcon';

const THRESHOLD_DISPLAY_NUMBER = 6;
const THRESHOLD_SMALL_ICONS = 5;
interface CostDisplayProps {
  cost: Cost;
}

function renderSimpleCostRune(costRune: SimpleCostRune, key?: string): ReactNode {
  return <RuneIcon key={key} runeType={costRune.type === 'any' ? ANY_RUNE : costRune.rune} />;
}

function renderCostRune(costRune: CostRune, key?: string): ReactNode {
  if (costRune.type === 'or') {
    return (
      <div key={key} className="rune-or">
        {renderSimpleCostRune(costRune.sub1)}
        {renderSimpleCostRune(costRune.sub2)}
      </div>
    );
  } else {
    return renderSimpleCostRune(costRune, key);
  }
}

export const CostDisplay: FC<CostDisplayProps> = ({ cost }) => {
  const content = cost.flatMap(({ costRune, count }, index) => {
    if (count === 'X' || count >= THRESHOLD_DISPLAY_NUMBER) {
      return (
        <div className="numbered-cost" key={index}>
          <div className="number">{count}</div>
          {renderCostRune(costRune)}
        </div>
      );
    } else {
      return Array.from({ length: count }, () => costRune).map((costRune, i) =>
        renderCostRune(costRune, `${index}-${i}`)
      );
    }
  });

  const classes = clsx('cost', { 'small-icons': content.length >= THRESHOLD_SMALL_ICONS });
  return <div className={classes}>{content}</div>;
};
