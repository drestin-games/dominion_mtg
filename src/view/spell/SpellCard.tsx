import clsx from 'clsx';
import React, { FC } from 'react';
import { SpellID, getSpell } from '../../game/spell/Spell';
import { canCastSpell } from '../../game/state/getters';
import { I18N } from '../i18n/i18n';
import { useGameContext } from '../state/GameContext';
import { isInStep } from '../state/getters';
import { ViewStep } from '../state/ViewState';
import { CostDisplay } from './CostDisplay';

interface SpellCardProps {
  spellID: SpellID;
}

export const SpellCard: FC<SpellCardProps> = ({ spellID }) => {
  const { G, view, selectSpell } = useGameContext();
  const spell = getSpell(spellID);

  const canSelect = isInStep(ViewStep.selectSpell, view);
  const valid = canCastSpell(G.turn.currentPlayerID, spell, G);
  const selected = view.selectedSpell === spellID;

  function onClick(): void {
    if (canSelect && valid) {
      selectSpell(spellID);
    }
  }

  const classes = clsx('spell-card', {
    selected: selected,
    selectable: canSelect && valid,
    'selectable-invalid': canSelect && !valid,
  });
  return (
    <div className={classes} onClick={() => onClick()}>
      <div className="title">{I18N.spell(spellID).title}</div>
      <div className="description">{I18N.spell(spellID).description}</div>
      <CostDisplay cost={spell.cost} />
    </div>
  );
};
