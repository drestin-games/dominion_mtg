import { EffectsBoardWrapper } from 'bgio-effects/react';
import { PlayerID } from 'boardgame.io';
import { BoardProps } from 'boardgame.io/react';
import React, { FC, createContext, useEffect, useState } from 'react';
import { CtxMovesType } from '../game/Game';
import { getAllPlayers } from '../game/state/getters';
import { MyGameState } from '../game/state/State';
import { Instruction } from './Instruction';
import { LogsPanel } from './LogsPanel';
import { EnchantmentList } from './modal/EnchantmentList';
import { EndGameModal } from './modal/EndGameModal';
import { ReserveDetailsModal } from './modal/ReserveDetailsModal';
import { SelectRuneTypeModal } from './modal/SelectRuneTypeModal';
import { PlayerBoard } from './player/PlayerBoard';
import { SpellsPanel } from './spell/SpellsPanel';
import { GameContext, useContextSource, useGameContext } from './state/GameContext';
import './style/style.scss';

interface ModalsControlContextProps {
  /** One ID to open, null to close */
  setEnchantmentListPlayerID: (playerID: PlayerID | null) => void;
  setReserveDetailsPlayerID: (playerID: PlayerID | null) => void;
}

export const ModalsControlContext = createContext<ModalsControlContextProps | null>(null);
const WrappedGameBoard: FC = () => {
  const { G, view } = useGameContext();
  const [enchantmentListPlayerID, setEnchantmentListPlayerID] = useState<PlayerID | null>(null);
  const [reserveDetailsPlayerID, setReserveDetailsPlayerID] = useState<PlayerID | null>(null);

  const modalContext: ModalsControlContextProps = {
    setEnchantmentListPlayerID,
    setReserveDetailsPlayerID,
  };

  useEffect(() => {
    if (enchantmentListPlayerID !== null) {
      setEnchantmentListPlayerID(null);
    }
    if (reserveDetailsPlayerID !== null) {
      setReserveDetailsPlayerID(null);
    }
  }, [view.step]);

  return (
    <ModalsControlContext.Provider value={modalContext}>
      <div className="board">
        <SpellsPanel />
        <Instruction />
        <div className="player-boards-container">
          {getAllPlayers(G).map(({ playerID }) => (
            <PlayerBoard key={playerID} playerID={playerID} />
          ))}
        </div>
        {view.error ? <div style={{ color: 'red', fontSize: 30 }}>{view.error ?? ''}</div> : null}
      </div>
      <LogsPanel />
      <SelectRuneTypeModal />
      <EnchantmentList playerID={enchantmentListPlayerID} />
      <ReserveDetailsModal playerID={reserveDetailsPlayerID} />
      <EndGameModal />
    </ModalsControlContext.Provider>
  );
};

type MyBoardProps = Omit<BoardProps<MyGameState<'mut'>>, 'moves'> & { moves: CtxMovesType };
const _MyGameBoard: FC<MyBoardProps> = (props) => {
  const initialGameContext = useContextSource(props);
  return (
    <GameContext.Provider value={initialGameContext}>
      <WrappedGameBoard />
    </GameContext.Provider>
  );
};

export const MyGameBoard = EffectsBoardWrapper(_MyGameBoard, { updateStateAfterEffects: true });
