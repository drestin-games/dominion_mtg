import { PlayerID } from 'boardgame.io';
import { RuneID } from '../../game/Rune';
import { onlyOneRuneCost } from '../../game/spell/Cost';
import { RuneParam } from '../../game/spell/ParamDef';
import { Spell, getSpell } from '../../game/spell/Spell';
import { canPaySpell, isInRange, isValidPlayer } from '../../game/state/getters';
import { MyGameState } from '../../game/state/State';
import { ViewState, ViewStep } from './ViewState';

/**
 * Return the Spell object for the current selected casting spell.
 */
export function getSelectedSpell(view: ViewState): Spell {
  if (view.selectedSpell === undefined) {
    throw new Error("Trying to get selected spell while there isn't any");
  }
  return getSpell(view.selectedSpell);
}

/**
 * If the current step is a step where we select hand runes.
 */
export function isHandRuneSelection(view: ViewState): boolean {
  return (
    isInStep(ViewStep.selectCastingRunes, view) ||
    isInStep(ViewStep.selectDiscardedRune, view) ||
    (isInStep(ViewStep.selectParams, view) && view.currentParamDef?.type === 'hand-runes')
  );
}

/**
 * If a rune in hand can be selected (for spell cost, discard move or hand-runes spell parameter).
 */
export function isHandRuneSelectable(playerID: PlayerID, runeID: RuneID, view: ViewState, G: MyGameState): boolean {
  const isCurrentPlayer = playerID === G.turn.currentPlayerID;

  if (isInStep(ViewStep.selectCastingRunes, view) && isCurrentPlayer) {
    const spell = getSelectedSpell(view);
    if (view.firstRuneOfRange === undefined) {
      return !onlyOneRuneCost(spell.cost) || canPaySpell(playerID, spell.id, { from: runeID, to: runeID }, G);
    }
    const range = {
      from: view.firstRuneOfRange,
      to: runeID,
    };
    return canPaySpell(playerID, spell.id, range, G);
  }
  if (isInStep(ViewStep.selectDiscardedRune, view) && isCurrentPlayer) {
    return true;
  }
  if (isInStep(ViewStep.selectParams, view) && view.currentParamDef?.type === 'hand-runes') {
    const paramDef = view.currentParamDef;
    if (!isValidPlayer(playerID, paramDef.who, G)) {
      return false;
    }
    const alreadySelected = isRuneSelected(playerID, runeID, view, G);
    // we can select each rune only once.
    return !alreadySelected;
  }
  return false;
}

/**
 * If a rune in reserve can be selected (for reserve-runes spell parameter).
 */
export function isReserveRuneSelectable(playerID: PlayerID, runeID: RuneID, view: ViewState, G: MyGameState): boolean {
  if (isInStep(ViewStep.selectParams, view) && view.currentParamDef?.type === 'reserve-runes') {
    const paramDef = view.currentParamDef;
    if (!isValidPlayer(playerID, paramDef.who, G)) {
      return false;
    }
    const alreadySelected = isRuneSelected(playerID, runeID, view, G);
    // we can select each rune only once.
    return !alreadySelected;
  }

  return false;
}

export function selectedSpellOneRuneCostOnly(view: ViewState): boolean {
  return view.step !== ViewStep.selectSpell && onlyOneRuneCost(getSelectedSpell(view).cost);
}

export function isRuneSelected(
  playerID: PlayerID,
  runeID: RuneID,
  view: ViewState,
  G: MyGameState
): false | 'selected' | 'selected-incomplete' {
  const isCurrentPlayer = playerID === G.turn.currentPlayerID;
  if (isInStep(ViewStep.selectCastingRunes, view) && isCurrentPlayer && view.firstRuneOfRange === runeID) {
    return 'selected-incomplete';
  }
  if (isCurrentPlayer && view.selectedRange && isInRange(playerID, view.selectedRange, runeID, G)) {
    return 'selected';
  }
  if (
    isInStep(ViewStep.selectParams, view) &&
    (view.currentParamDef?.type === 'hand-runes' || view.currentParamDef?.type === 'reserve-runes')
  ) {
    if (
      view.currentParam &&
      (view.currentParam as RuneParam[]).find((p) => p.playerID === playerID && p.runeID === runeID)
    ) {
      return 'selected-incomplete';
    }
  }
  return false;
}

/**
 * Return if the view is in the given step (and not waiting for game calculations)
 */
export function isInStep(step: ViewStep, view: ViewState): boolean {
  return !view.waiting && view.step === step;
}

export function isSelectingRuneTypes(view: ViewState): boolean {
  return (
    (isInStep(ViewStep.selectParams, view) && view.currentParamDef?.type === 'basic-runes') ||
    isInStep(ViewStep.selectGifts, view)
  );
}
