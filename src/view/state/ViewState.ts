import { RuneID } from '../../game/Rune';
import { ParamDef } from '../../game/spell/ParamDef';
import { SpellID, SpellParams } from '../../game/spell/Spell';
import { HandRange } from '../../game/state/getters';
import { Access, Mutability } from '../../game/utils';

export enum ViewStep {
  selectSpell = 'selectSpell',
  selectCastingRunes = 'selectCastingRunes',
  selectParams = 'selectParams',
  selectDiscardedRune = 'selectDiscardedRune',
  selectGifts = 'selectGifts',
}

interface ViewStateMut {
  step?: ViewStep;
  selectedSpell?: SpellID;
  selectedRange?: HandRange;
  /** ParamDef we currently try to fill */
  currentParamName?: string;
  currentParamDef?: ParamDef;
  /** Intermediate value of paramDef. For example an array when we select multiple runes */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  currentParam?: Array<any>;
  /** Range of runes during spell cost selection */
  firstRuneOfRange?: RuneID;
  spellParams: SpellParams;
  error?: string;
  /** Waiting for animations to complete or another player to play */
  waiting: boolean;
}

export type ViewState<M extends Mutability = 'const'> = Access<ViewStateMut, M>;

export function initialViewState(): ViewState {
  return {
    step: undefined,
    spellParams: {},
    waiting: true,
  };
}
