import { PlayerID } from 'boardgame.io';
import { BoardProps } from 'boardgame.io/react';
import { createContext, useContext, useEffect, useState } from 'react';
import { CastSpellArgs, checkOneSpellParam } from '../../game/action/moves';
import { useMyBuiltinEffectListener } from '../../game/effects';
import { CtxMovesType } from '../../game/Game';
import { RuneID, RuneType } from '../../game/Rune';
import { onlyOneRuneCost } from '../../game/spell/Cost';
import { HandRunesParamType, ReserveRunesParamType, RuneParam } from '../../game/spell/ParamDef';
import { SpellID, getSpell } from '../../game/spell/Spell';
import {
  HandRange,
  canCastSpell,
  canPaySpell,
  getHandRunes,
  getOponents,
  isValidRange,
  rangeToString,
} from '../../game/state/getters';
import { MyGameState } from '../../game/state/State';
import { deepCopy } from '../../game/utils';
import { getSelectedSpell, isHandRuneSelectable, isInStep, isReserveRuneSelectable } from './getters';
import { ViewState, ViewStep, initialViewState } from './ViewState';

type MyBoardProps = Omit<BoardProps<MyGameState>, 'moves' | 'ctx'> & { moves: CtxMovesType; ctx: ClientContext };

type ViewAction<Args extends unknown[]> = (props: MyBoardProps, view: ViewState<'mut'>, ...args: Args) => void;

function _emptyView(view: ViewState<'mut'>): void {
  view.step = undefined;
  view.selectedSpell = undefined;
  view.selectedRange = undefined;
  view.currentParamDef = undefined;
  view.currentParamName = undefined;
  view.currentParam = undefined;
  view.firstRuneOfRange = undefined;
  view.spellParams = {};
  view.waiting = true;
}

function resetView(view: ViewState<'mut'>, G: MyGameState, ctx: ClientContext, waitingForGame = true): void {
  _emptyView(view);
  if (G.turn.phase === 'runeGifts') {
    view.step = ViewStep.selectGifts;
    view.currentParamDef = G.turn.paramDef;
    view.currentParamName = 'gifts';
    view.currentParam = [];
  } else {
    view.step = ViewStep.selectSpell;
  }
  const waitingForPlayer = ctx.playerID !== null && ctx.playerID !== G.turn.currentPlayerID;
  view.waiting = waitingForPlayer || waitingForGame;
}

const cancel: ViewAction<[]> = ({ G, ctx }, view) => resetView(view, G, ctx, false);

const passTurn: ViewAction<[]> = ({ moves, G, ctx }, view): void => {
  resetView(view, G, ctx);
  moves.passTurn();
};

const shuffleHand: ViewAction<[]> = ({ moves, G, ctx }, view): void => {
  if (view.step !== ViewStep.selectSpell) {
    throw new Error('Invalid state: shuffling hand in the wrong step');
  }
  resetView(view, G, ctx);
  moves.shuffleHand();
};

const chooseDiscardMove: ViewAction<[]> = (_, view): void => {
  if (view.step !== ViewStep.selectSpell) {
    throw new Error('Invalid state: choosing the discard move in the wrong step');
  }
  view.step = ViewStep.selectDiscardedRune;
};

function selectDiscardedRune(
  runeID: RuneID,
  moves: CtxMovesType,
  view: ViewState<'mut'>,
  G: MyGameState,
  ctx: ClientContext
): void {
  if (view.step !== ViewStep.selectDiscardedRune) {
    throw new Error('Invalid state: selecting the discarded rune in the wrong step');
  }
  moves.discardMove(runeID);
  resetView(view, G, ctx);
}

const selectSpell: ViewAction<[SpellID]> = ({ G }, view, spellID): void => {
  if (view.step !== ViewStep.selectSpell) {
    throw new Error('Invalid state: selecting spell in the wrong step');
  }
  if (!canCastSpell(G.turn.currentPlayerID, getSpell(spellID), G)) {
    throw new Error("Invalid state: selecting a spell that can't be casted");
  }
  view.selectedSpell = spellID;
  view.step = ViewStep.selectCastingRunes;
};

function selectCastingRunes(
  range: HandRange,
  moves: CtxMovesType,
  view: ViewState<'mut'>,
  G: MyGameState,
  ctx: ClientContext
): void {
  if (view.selectedSpell === undefined) {
    throw new Error('Invalid state: selecting runes without having selected a spell');
  }
  if (view.step != ViewStep.selectCastingRunes) {
    throw new Error('Selecting runes in the wrong step');
  }

  const playerID = G.turn.currentPlayerID;
  if (!isValidRange(playerID, range, G)) {
    throw new Error(`Player ${playerID} can't pay runes ${rangeToString(range)}`);
  }

  if (!canPaySpell(playerID, view.selectedSpell, range, G)) {
    const usedRunes = getHandRunes(playerID, range, G);
    throw new Error(`${JSON.stringify(usedRunes)} can't pay for ${JSON.stringify(getSelectedSpell(view).cost)}`);
  }

  view.selectedRange = range;
  view.firstRuneOfRange = undefined;
  advanceSpellCastingProgress(view, G, ctx, moves);
}

const selectHandRune: ViewAction<[RuneParam]> = ({ moves, G, ctx }, view, { playerID, runeID }) => {
  if (!isHandRuneSelectable(playerID, runeID, view, G)) {
    throw new Error('Illegal state: Selecting an invalid rune');
  }
  if (isInStep(ViewStep.selectCastingRunes, view)) {
    const spell = getSelectedSpell(view);
    if (onlyOneRuneCost(spell.cost)) {
      selectCastingRunes({ from: runeID, to: runeID }, moves, view, G, ctx);
    } else if (view.firstRuneOfRange === undefined) {
      view.firstRuneOfRange = runeID;
    } else {
      const range = { from: view.firstRuneOfRange, to: runeID };
      selectCastingRunes(range, moves, view, G, ctx);
    }
  } else if (isInStep(ViewStep.selectDiscardedRune, view)) {
    selectDiscardedRune(runeID, moves, view, G, ctx);
  } else if (isInStep(ViewStep.selectParams, view)) {
    if (view.currentParam === undefined) {
      view.currentParam = [];
    }
    const newRuneParam: RuneParam = {
      playerID,
      runeID: runeID,
    };
    view.currentParam.push(newRuneParam);

    if (view.currentParam.length === (view.currentParamDef as HandRunesParamType).count) {
      _selectParam(view.currentParam, moves, view, G, ctx);
    }
  }
};

const selectReserveRune: ViewAction<[RuneParam]> = ({ moves, G, ctx }, view, { playerID, runeID }) => {
  if (!isReserveRuneSelectable(playerID, runeID, view, G)) {
    throw new Error('Illegal state: Selecting an invalid rune');
  }
  if (isInStep(ViewStep.selectParams, view)) {
    if (view.currentParam === undefined) {
      view.currentParam = [];
    }
    const newRuneParam: RuneParam = {
      playerID,
      runeID: runeID,
    };
    view.currentParam.push(newRuneParam);

    if (view.currentParam.length === (view.currentParamDef as ReserveRunesParamType).count) {
      _selectParam(view.currentParam, moves, view, G, ctx);
    }
  }
};

/**
 * Handle the cases when there is only one possible value to the current spell parameter.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function autoCompleteParam(view: ViewState<'mut'>, G: MyGameState): any {
  const paramDef = view.currentParamDef!;

  if (paramDef.type === 'player' && paramDef.oponentOnly) {
    const oponents = getOponents(G.turn.currentPlayerID, G);
    if (oponents.length === 1) {
      return oponents[0];
    }
  }
}

/**
 * Prepare the next spell param and try to autofill it.
 * If all params are filled, return true.
 */
function advanceSpellCastingProgress(
  view: ViewState<'mut'>,
  G: MyGameState,
  ctx: ClientContext,
  moves: CtxMovesType
): void {
  if (!view.selectedSpell && !view.selectedRange) {
    throw new Error('Invalid state: trying to fill spell params before selecting runes or spell');
  }

  const spell = getSelectedSpell(view);

  for (const [paramName, paramDef] of Object.entries(spell.paramsDef)) {
    if (view.spellParams[paramName] === undefined) {
      view.step = ViewStep.selectParams;
      view.currentParamDef = paramDef;
      view.currentParamName = paramName;
      view.currentParam = undefined;
      const autoComplete = autoCompleteParam(view, G);
      if (autoComplete !== undefined) {
        view.spellParams[view.currentParamName] = autoComplete;
      } else {
        return;
      }
    }
  }

  // If we get here, it means all the params are filled: cast the spell
  view.currentParamDef = undefined;
  const params: CastSpellArgs = {
    spellID: view.selectedSpell!,
    range: view.selectedRange!,
    params: view.spellParams,
  };
  moves.castSpell(params);
  resetView(view, G, ctx);
}

function _selectParam(
  paramValue: unknown,
  moves: CtxMovesType,
  view: ViewState<'mut'>,
  G: MyGameState,
  ctx: ClientContext
): void {
  if (view.step !== ViewStep.selectParams && view.step !== ViewStep.selectGifts) {
    throw new Error('Selecting spell params in the wrong step');
  }
  if (!view.currentParamDef || !view.currentParamName) {
    throw new Error('Selecting unknown spell params');
  }

  if (!checkOneSpellParam(view.currentParamDef, paramValue, G)) {
    throw new Error(`Invalid param selection (${JSON.stringify(view.currentParamDef)}): ${JSON.stringify(paramValue)}`);
  }

  if (view.step === ViewStep.selectParams) {
    view.spellParams[view.currentParamName] = paramValue;
    advanceSpellCastingProgress(view, G, ctx, moves);
  } else if (view.step === ViewStep.selectGifts) {
    moves.submitGiftsChoice(paramValue as RuneType[]);
    resetView(view, G, ctx);
  }
}

const selectParam: ViewAction<[unknown]> = ({ G, ctx, moves }, view, paramValue): void => {
  _selectParam(paramValue, moves, view, G, ctx);
};

export const GameContext = createContext<GameContextType | undefined>(undefined);

type GameContextType = {
  G: MyGameState;
  ctx: ClientContext;
  view: ViewState;
  cancel: () => void;
  passTurn: () => void;
  selectSpell: (spellID: SpellID) => void;
  selectHandRune: (param: RuneParam) => void;
  selectReserveRune: (param: RuneParam) => void;
  selectParam: (value: unknown) => void;
  shuffleHand: () => void;
  chooseDiscardMove: () => void;
};

export interface ClientContext {
  playerID: PlayerID | null;
  matchData:
    | {
        id: number;
        name?: string;
      }[]
    | undefined;
}

function clientContext({ playerID, matchData }: BoardProps<MyGameState>): ClientContext {
  return { playerID, matchData };
}

function tweakBoardProps(props: BoardProps<MyGameState>): MyBoardProps {
  return { ...props, moves: props.moves as CtxMovesType, ctx: clientContext(props) };
}

export function useContextSource(props: BoardProps<MyGameState>): GameContextType {
  const myProps = tweakBoardProps(props);
  const [_view, _setView] = useState<ViewState>(initialViewState());

  function runnableAction<Args extends unknown[]>(f: ViewAction<Args>): (...args: Args) => void {
    return (...args) => {
      // Like immer produce. With deep copy instead of proxies (which were causing problems)
      const view: ViewState<'mut'> = deepCopy<ViewState>(_view) as ViewState<'mut'>;
      try {
        f(myProps, view, ...args);
      } catch (e) {
        resetView(view, props.G, myProps.ctx);
        view.error = String(e);
      }
      _setView(view);
    };
  }

  const deps = [props._stateID];
  useEffect(() => {
    runnableAction(({ G, ctx }, view) => resetView(view, G, ctx, false))();
  }, deps);

  useMyBuiltinEffectListener('effects:start', () => runnableAction((_, view) => (view.waiting = true))(), deps);
  useMyBuiltinEffectListener(
    'effects:end',
    () => runnableAction(({ G, ctx }, view) => resetView(view, G, ctx))(),
    deps
  );

  return {
    G: props.G,
    ctx: clientContext(props),
    view: _view,
    cancel: runnableAction(cancel),
    passTurn: runnableAction(passTurn),
    selectSpell: runnableAction(selectSpell),
    selectParam: runnableAction(selectParam),
    shuffleHand: runnableAction(shuffleHand),
    chooseDiscardMove: runnableAction(chooseDiscardMove),
    selectHandRune: runnableAction(selectHandRune),
    selectReserveRune: runnableAction(selectReserveRune),
  };
}

export function useGameContext(): GameContextType {
  return useContext(GameContext)!;
}
